var express = require('express');
var https = require('https');
var http = require('http');
var colors = require('colors');
var logger = require('./server/config/logger');
var request = require('request');

/* Load this before loading the config file otherwise environment variables won't be ready */
logger.info('configuring environment variables');
require('dotenv').config()

logger.info('configuring express');
var app = express();
require('./server/config/express')(app);

logger.info('Configuring mongoose'.grey);
require('./server/config/mongoose')();

/*logger.info('configuring passport');
require('./server/config/passport')();*/

logger.info('configuring vendor libraries');
require('./server/config/vendor').vendorConfiguration();

logger.info('configuring routes');
require('./server/routes/routes')(app);

/*logger.info('configuring scheduler');
require('./server/config/scheduler')();*/

var config = require('./server/config/config');
logger.info('configuring listener for http on port: ' + config.http.port);
app.listen(config.http.port);

/*if (config.env === 'development') {
  logger.info('configuring listener for https on port: ' + config.https.port);
  https.createServer(config.https.options, app).listen(config.https.port);
}*/

logger.info("Listening on port " + config.http.port);

module.exports = app;
