module.exports = {
  "artists": {
    "href": "https://api.spotify.com/v1/search?query=%E2%80%9Dtania+bowra%E2%80%9D&offset=0&limit=3&type=artist",
    "items": [
      {
        "external_urls": {
          "spotify": "https://open.spotify.com/artist/08td7MxkoHQkXnWAYD8d6Q"
        },
        "followers": {
          "href": null,
          "total": 41
        },
        "genres": [], //Event.performers.artist.genres
        "href": "https://api.spotify.com/v1/artists/08td7MxkoHQkXnWAYD8d6Q",
        "id": "08td7MxkoHQkXnWAYD8d6Q", //Event.performers.artist.spotify.id
        "images": [ //Event.performers.artist.image
          {
            "height": 640,
            "url": "https://i.scdn.co/image/a529b65b4bd322b16bee34672ce45278e890e176",
            "width": 640
          }],
        "name": "Tania Bowra",
        "popularity": 2,
        "type": "artist",
        "uri": "spotify:artist:08td7MxkoHQkXnWAYD8d6Q"
      }
    ],
    "limit": 3,
    "next": null,
    "offset": 0,
    "previous": null,
    "total": 1
  }
}
