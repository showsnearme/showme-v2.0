module.exports = {
  "resultsPage": {
    "status": "ok",
    "results": {
      "event": [
        {
          "type": "Concert",
          "status": "ok",
          "popularity": 0,
          "displayName": "In the Vane Of... at Rams Head On Stage (September 28, 2016)",
          "start": {
            "datetime": "2016-09-28T19:00:00-0400",
            "time": "19:00:00",
            "date": "2016-09-28"
          },
          "ageRestriction": null,
          "location": {
            "city": "Annapolis, MD, US",
            "lat": 38.978307,
            "lng": -76.4946279
          },
          "uri": "http://www.songkick.com/concerts/27953394-in-the-vane-of-at-rams-head-on-stage?utm_source=41371&utm_medium=partner",
          "id": 27953394,
          "performance": [
            {
              "billingIndex": 1,
              "artist": {
                "displayName": "In the Vane Of...",
                "uri": "http://www.songkick.com/artists/7860289-in-the-vane-of?utm_source=41371&utm_medium=partner",
                "identifier": [],
                "id": 7860289
              },
              "displayName": "In the Vane Of...",
              "id": 54431249,
              "billing": "headline"
            }
          ],
          "venue": {
            "metroArea": {
              "displayName": "Baltimore",
              "uri": "http://www.songkick.com/metro_areas/4125-us-baltimore?utm_source=41371&utm_medium=partner",
              "country": {
                "displayName": "US"
              },
              "id": 4125,
              "state": {
                "displayName": "MD"
              }
            },
            "displayName": "Rams Head On Stage",
            "lat": 38.978307,
            "lng": -76.4946279,
            "uri": "http://www.songkick.com/venues/522876-rams-head-on-stage?utm_source=41371&utm_medium=partner",
            "id": 522876
          }
        },
        {
          "type": "Concert",
          "status": "ok",
          "popularity": 0.009843,
          "displayName": "Julianna Barwick with Mary Lattimore at Metro Gallery (September 28, 2016)",
          "start": {
            "datetime": "2016-09-28T21:30:00-0400",
            "time": "21:30:00",
            "date": "2016-09-28"
          },
          "ageRestriction": null,
          "location": {
            "city": "Baltimore, MD, US",
            "lat": 39.3088401,
            "lng": -76.6163947
          },
          "uri": "http://www.songkick.com/concerts/27760384-julianna-barwick-at-metro-gallery?utm_source=41371&utm_medium=partner",
          "id": 27760384,
          "performance": [
            {
              "billingIndex": 1,
              "artist": {
                "displayName": "Julianna Barwick",
                "uri": "http://www.songkick.com/artists/796444-julianna-barwick?utm_source=41371&utm_medium=partner",
                "identifier": [
                  {
                    "href": "http://api.songkick.com/api/3.0/artists/mbid:ba49bbe3-188b-44d7-95f3-92ff9939af28.json",
                    "mbid": "ba49bbe3-188b-44d7-95f3-92ff9939af28"
                  }
                ],
                "id": 796444
              },
              "displayName": "Julianna Barwick",
              "id": 54075389,
              "billing": "headline"
            },
            {
              "billingIndex": 2,
              "artist": {
                "displayName": "Mary Lattimore",
                "uri": "http://www.songkick.com/artists/2403543-mary-lattimore?utm_source=41371&utm_medium=partner",
                "identifier": [
                  {
                    "href": "http://api.songkick.com/api/3.0/artists/mbid:5e2c9a0d-d21d-4b56-8bdb-43e01169c91b.json",
                    "mbid": "5e2c9a0d-d21d-4b56-8bdb-43e01169c91b"
                  }
                ],
                "id": 2403543
              },
              "displayName": "Mary Lattimore",
              "id": 54075394,
              "billing": "support"
            }
          ],
          "venue": {
            "metroArea": {
              "displayName": "Baltimore",
              "uri": "http://www.songkick.com/metro_areas/4125-us-baltimore?utm_source=41371&utm_medium=partner",
              "country": {
                "displayName": "US"
              },
              "id": 4125,
              "state": {
                "displayName": "MD"
              }
            },
            "displayName": "Metro Gallery",
            "lat": 39.3088401,
            "lng": -76.6163947,
            "uri": "http://www.songkick.com/venues/67690-metro-gallery?utm_source=41371&utm_medium=partner",
            "id": 67690
          }
        },
        {
          "type": "Concert",
          "status": "ok",
          "popularity": 0.000015,
          "displayName": "Will Ready at Hippodrome Theatre, France-Merrick Performing Arts Center (September 28, 2016)",
          "start": {
            "datetime": "2016-09-28T16:30:00-0400",
            "time": "16:30:00",
            "date": "2016-09-28"
          },
          "ageRestriction": null,
          "location": {
            "city": "Baltimore, MD, US",
            "lat": 39.2896248,
            "lng": -76.6210703
          },
          "uri": "http://www.songkick.com/concerts/24157834-will-ready-at-hippodrome-theatre-francemerrick-performing-arts-center?utm_source=41371&utm_medium=partner",
          "id": 24157834,
          "performance": [
            {
              "billingIndex": 1,
              "artist": {
                "displayName": "Will Ready",
                "uri": "http://www.songkick.com/artists/6637769-will-ready?utm_source=41371&utm_medium=partner",
                "identifier": [],
                "id": 6637769
              },
              "displayName": "Will Ready",
              "id": 47645114,
              "billing": "headline"
            }
          ],
          "venue": {
            "metroArea": {
              "displayName": "Baltimore",
              "uri": "http://www.songkick.com/metro_areas/4125-us-baltimore?utm_source=41371&utm_medium=partner",
              "country": {
                "displayName": "US"
              },
              "id": 4125,
              "state": {
                "displayName": "MD"
              }
            },
            "displayName": "Hippodrome Theatre, France-Merrick Performing Arts Center",
            "lat": 39.2896248,
            "lng": -76.6210703,
            "uri": "http://www.songkick.com/venues/498916-hippodrome-theatre-francemerrick-performing-arts-center?utm_source=41371&utm_medium=partner",
            "id": 498916
          }
        },
        {
          "type": "Concert",
          "status": "ok",
          "popularity": 0.00003,
          "displayName": "Underlined Passages with Classified Frequency and Curse Words (DC) at Ottobar (September 28, 2016)",
          "start": {
            "datetime": "2016-09-28T20:30:00-0400",
            "time": "20:30:00",
            "date": "2016-09-28"
          },
          "ageRestriction": null,
          "location": {
            "city": "Baltimore, MD, US",
            "lat": 39.3188012,
            "lng": -76.6196946
          },
          "uri": "http://www.songkick.com/concerts/27965459-underlined-passages-at-ottobar?utm_source=41371&utm_medium=partner",
          "id": 27965459,
          "performance": [
            {
              "billingIndex": 1,
              "artist": {
                "displayName": "Underlined Passages",
                "uri": "http://www.songkick.com/artists/8501613-underlined-passages?utm_source=41371&utm_medium=partner",
                "identifier": [],
                "id": 8501613
              },
              "displayName": "Underlined Passages",
              "id": 54452879,
              "billing": "headline"
            },
            {
              "billingIndex": 2,
              "artist": {
                "displayName": "Classified Frequency",
                "uri": "http://www.songkick.com/artists/1133200-classified-frequency?utm_source=41371&utm_medium=partner",
                "identifier": [],
                "id": 1133200
              },
              "displayName": "Classified Frequency",
              "id": 54452884,
              "billing": "support"
            },
            {
              "billingIndex": 3,
              "artist": {
                "displayName": "Curse Words (DC)",
                "uri": "http://www.songkick.com/artists/8566724-curse-words-dc?utm_source=41371&utm_medium=partner",
                "identifier": [],
                "id": 8566724
              },
              "displayName": "Curse Words (DC)",
              "id": 54452889,
              "billing": "support"
            }
          ],
          "venue": {
            "metroArea": {
              "displayName": "Baltimore",
              "uri": "http://www.songkick.com/metro_areas/4125-us-baltimore?utm_source=41371&utm_medium=partner",
              "country": {
                "displayName": "US"
              },
              "id": 4125,
              "state": {
                "displayName": "MD"
              }
            },
            "displayName": "Ottobar",
            "lat": 39.3188012,
            "lng": -76.6196946,
            "uri": "http://www.songkick.com/venues/38549-ottobar?utm_source=41371&utm_medium=partner",
            "id": 38549
          }
        }
      ]
    },
    "perPage": 50,
    "page": 1,
    "totalEntries": 4
  }
}
