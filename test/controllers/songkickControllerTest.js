var chai = require('chai');
var expect = require('chai').expect;
var assert = require('chai').assert;

var songkickController = require('./songkickControllerMock');
var Stopwatch = require("node-stopwatch").Stopwatch;



xdescribe('songkickController: function getEvents', function(){
    var songkickEventsPromise;

    beforeEach(function(done){
        console.log('"Querying" songkick api..')
        setTimeout(function(){
             songkickEventsPromise = songkickController.getEvents();
             console.log('Network call complete..')
             done();
        }, 1000)
    });

    it('should return an promise of songkickEvents data', function(){
      songkickEventsPromise.then(function(data){
          assert.isArray(data.resultsPage.results.event, 'songkick events data returned & array exists');
      }, function(err){
          assert.isUndefined(err, 'no error occured when getting songkick data');
      })
    })
});

xdescribe('songkickController: function createEventModel', function(){
  var songkickEvent = require('../apis/songkick/data/Events.js').resultsPage.results.event[0]

  it('should return an event model populated with key values', function(){
      var eventModel = songkickController.createEventModel(songkickEvent);
      expect(eventModel).to.exist;

      assert.isDefined(eventModel.performers[0].displayName, 'At least 1 artist has been defined');
      assert.isDefined(eventModel.start.displayDate, 'Event start time has been defined');
      assert.isDefined(eventModel.venue.displayName, 'Even .venue display name has been defined');
      assert.isDefined(eventModel.venue.lat, 'Event venue latitude has been defined');
      assert.isDefined(eventModel.venue.lng, 'Event venue longitude has been defined');


  })
});
