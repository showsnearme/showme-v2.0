var Event = require('../../server/models/Event')
var songkickapi = require('../apis/songkick/songkick_endpoints')
var songkickController = require('../../server/controllers/songkickController.js');
var Promise = require('promise')

exports.getLocation = function getLocation(req,res){
    //TODO
};

exports.getEvents = function getEvent(min_date,max_date, songkick_locationid){
    var promise = new Promise(function (resolve, reject) {

        var data = songkickapi.getEvents();
        if (!data) reject('No songkick data found');
        else resolve(data);
    });

    return promise;
};

exports.createEventModel = songkickController.createEventModel;
