# **About the app** #

Showme is an app that uses the Songkick API to search for artists that are coming to play live in a particular location. It then uses the Spotify API to generate a playlist based on those artists.

For example, if I have a trip planned to New York for a weekend in November I can search for artists playing that weekend, generate a playlist of those artists, and listen before my trip. If I like one of the artists from the playlist, I now have the immediate option of going to see them live. 


# **Running the app**
 #
Clone project 
` git clone __`  --> copy the link in upper right corner

`cd` into `showme2.0` directory

```
npm install
bower install
gulp serve
```

Note you must have gulp installed to run the app
```
npm install -g gulp
```

# **Configuration notes** #

This project uses gulp as the task runner, gulp-nodemon for server-reload on changes.