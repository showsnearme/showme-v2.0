var logger = require('../config/logger');
var songkickController = require('../controllers/songkickController.js');
var spotifyService = require('./spotifyService.js');
var songkickService = require('./songkickService.js');
var Rx = require('rx');
var eachLimit = require('async').eachLimit;


exports.getEventsForALocation = function getEventsForALocation(parameters, returnEventsToClient){
    var Events = [], TransformedEvents = [], songkickObservable, songkickDataSource;

    const { location_info, min_date, max_date, access_token, page } = parameters;

    if (location_info.songkick_locationid){
        songkickObservable = Rx.Observable.fromNodeCallback(songkickController.events);
        songkickDataSource = songkickObservable(min_date, max_date, location_info.songkick_locationid, page);
    }

    if (location_info.lat && location_info.lat){
        songkickObservable = Rx.Observable.fromNodeCallback(songkickController.getEventsByLatitudeAndLongitude);
        songkickDataSource = songkickObservable(min_date, max_date, location_info.lat, location_info.lng, page);
    }

    logger.info('Creating event object from songkick data.');
    songkickDataSource.subscribe(
        (events) => {
            Events = events;
        },
        (err) =>{
            returnEventsToClient(null, err);
            logger.error('Unable to get songkick events', err)
        },
        () => {
            if (Events.resultsPage.totalEntries > 0){
                var metaData = getMetaData(Events, location_info.songkick_locationid);
                Events = Events.resultsPage.results.event;
                Events = Events.map(songkick_event => songkickService.createEventModelFrom(songkick_event, access_token))
                .filter( (songkick_event) => {
                    //TODO: clean up other important data here
                    if (songkick_event.performers.length > 0){
                        return songkick_event
                    }
                });

                logger.info('Mapping spotify artists to event object.');
                eachLimit(Events, 5, function iteratee(event, iterationDone){
                    spotifyService.addSpotifyArtistsToEventModel(event, access_token, function(err, transformedEvent){
                        if (err){
                          logger.error('Spotify artists NOT successfully added to event object', err);
                          iterationDone(err);
                        }
                        logger.debug('Spotify artists successfully added to event object');
                        TransformedEvents.push(transformedEvent);
                        iterationDone();
                    });

                }, function allIterationsDone(err){
                    if (!err){
                        logger.info('Songkick and spotify information successfully combined.');
                        returnEventsToClient(null, {"events": TransformedEvents, "metaData": metaData})
                    }else{
                        logger.error(err)
                    }
                });
            }else{
                returnEventsToClient(null, 'There were no events found.')
            }
        }
    );
};


var getMetaData = function(Events, songkick_locationid){
    var numberOfResults = Events.resultsPage.totalEntries;
    var resultsPerPage = parseInt(Events.resultsPage.perPage);
    var currentPage =  Events.resultsPage.page;
    var numberOfPages = Math.ceil(numberOfResults/resultsPerPage);

    if (Events.resultsPage.clientLocation){
        songkick_locationid = Events.resultsPage.clientLocation.metroAreaId
    }

    return {
        numberOfResults : numberOfResults,
        numberOfPages : numberOfPages,
        songkick_locationid : songkick_locationid,
        currentPage: currentPage
    }
}
