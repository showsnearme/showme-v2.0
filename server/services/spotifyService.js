var logger = require('../config/logger');
var mapLimit = require('async').mapLimit;
var stringUtil = require('../utilities/stringUtil')
var spotifyController = require('../controllers/spotifyController.js');
var Rx = require('rx');
var querystring = require('querystring');
var config = require('../config/config');
var async = require('async');
var _ = require('underscore');


exports.addSpotifyArtistsToEventModel = function addSpotifyArtistsToEventModel(event, access_token, callback){
    var spotifyObservable = Rx.Observable.fromNodeCallback(spotifyController.findArtistOnSpotify);

    mapLimit(event.performers, 5, function iteratee(performer, transformationDone){
        var spotifyDataSource = spotifyObservable(access_token, performer.displayName);

        var spotifyArtist;
        spotifyDataSource.subscribe(
            (artist) => {
                spotifyArtist = artist;
            },
            (err) =>{
                logger.error(err)
                transformationDone(err)
            },
            () => {
                var updated_performer = {};

                //copy whatever is already there
                for (prop in performer){
                    updated_performer[prop] = performer[prop]
                }

                //add new spotify information
                if (spotifyArtist && spotifyArtist.artists && spotifyArtist.artists.items[0]){
                    updated_performer.isAvailableOnSpotify = spotifyArtist.artists.items[0].id ? true : false;
                    updated_performer.spotify = {
                        artist_id: spotifyArtist.artists.items[0].id,
                        displayName: spotifyArtist.artists.items[0].name
                    };
                    updated_performer.images = spotifyArtist.artists.items[0].images;
                    updated_performer.popularity = spotifyArtist.artists.items[0].popularity;
                    updated_performer.genres = spotifyArtist.artists.items[0].genres;
                }

                transformationDone(null, updated_performer)
            }
        );


    }, function allTransformationsDone(err, transformedPerformers){
        if (!err){
            event.performers = transformedPerformers;
            callback(null, event)
        }else{
            logger.error('Error')
            callback(err)
        }
    });
};


var getTopTracksForAllSelectedArtists = function getTopTracksForAllSelectedArtists(artist_ids, user, callback){
    logger.info('Getting top tracks for selected artists');
    const { access_token, country } = user;
    async.mapLimit(artist_ids, 10, function iteratee(artist_id, transformationDone){

        spotifyController.getTopTracks(access_token, artist_id, country, (err, top_track_uris) => {
            if (!err){
                logger.debug('Successfully received top track uris', top_track_uris)
                transformationDone(null, top_track_uris);
            }else{
                logger.error('Error trying to find ' + artist_id + ' top tracks')
                transformationDone('Error trying to find ' + artist_id + ' top tracks')
            }

        });
    }, function allTransformationsDone(err, top_track_uris){
        if (!err){
            logger.info('Successfully received top tracks for selected artists');
            callback(null, top_track_uris);
        }else{
            logger.error('Error trying to find artists top tracks ', err);
            callback('Error trying to find artists top tracks ', err);
        }
    })
}

exports.getPlaylistUri = function createPlaylist(req, res, callback){
    var user = req.session.user;

    //Need to bind context here, https://github.com/caolan/async/issues/431
    logger.info('Going to create playlist and get top tracks for artists.')
    var task1 = getTopTracksForAllSelectedArtists.bind(null, req.session.playlist_info.selectedArtistIds, user);
    var task2 = spotifyController.createPlaylist.bind(null, user.access_token, user.id, req.session.playlist_info.playlistName, req.session.playlist_info.spotify_description_without_line_breaks);

    var this_component = this;
    async.parallel([task1, task2], function(err, results){
        if (!err){
            var track_uris = _.flatten(results[0]);
            var playlist = results[1];

            logger.info('Adding top tracks to newly created playlist.')
            spotifyController.addTracksToPlaylist(user.access_token, track_uris, user.id, playlist.id, (err, success) =>{
                if (!err){
                    callback(null, playlist)
                }else{
                    callback(err)
                }
            })
        }else{
            callback(err)
        }
    });
}

exports.authorizeUser = function(req, res) {
    req.session.state = stringUtil.generateRandomString(16);

    res.redirect('https://accounts.spotify.com/authorize?' +
        querystring.stringify({
            client_id: config.spotify.client_id,
            response_type: 'code',
            state: req.session.state,
            redirect_uri: config.spotify.redirect_uri,  /*/api/spotify/token*/
            scope: 'user-read-private playlist-modify-public',
        }));
};
