var moment = require('moment');
var Performer = require('../models/Performer');
var Venue = require('../models/Venue');
var Start = require('../models/Start');

exports.buildLocationsArray = function buildLocationsArray(locationResults){
    try{
        locationResults = locationResults.slice(0,15);
        var locations = [];
        locationResults.forEach( function buildLocationsObj (loc){

            var lat = loc.city.lat ? loc.city.lat : "";
            var lng = loc.city.lng ? loc.city.lng : "";
            var city = loc.city ? loc.city.displayName : "";
            var state = loc.city.state ? ", " + loc.city.state.displayName : "";
            var metroArea = loc.metroArea ? " " + loc.metroArea.displayName : "";
            var country = loc.city.country ? " " + loc.city.country.displayName : "";
            var id = loc.metroArea.id;
            var displayName = city + state + metroArea + country;
            var selected = false;

            var locationObj = {
                city: city,
                lat: lat,
                lng: lng,
                state: state,
                metroArea: metroArea,
                country: country,
                id: id,
                displayName: displayName,
                selected: selected
            };

            locations.push(locationObj);

        });
    }catch(e){
        var feedback = {
            city: 'no locations ',
            state: 'found'
        };
        locations = [feedback];
    }
    return locations
};

exports.createEventModelFrom = function createEventModelFrom(songkickEvent){
    var _event = Object.create(songkickEvent);
    _event.performers = this.initializePerformersModel(songkickEvent);
    _event.start = this.initializeStartModel(songkickEvent);
    _event.venue = this.initializeVenueModel(songkickEvent);
    _event.id = songkickEvent.id;

    return _event;
};

exports.initializePerformersModel =  function initializePerformersModel(songkickEvent){
    var _performers = [];

    songkickEvent.performance.forEach(function(songkick_performance){
        var _performer = Object.create(Performer);
        _performer.displayName = songkick_performance.artist.displayName;

        _performer.songkick = {
            artist_id: songkick_performance.artist.id,
            displayName: songkick_performance.artist.displayName,
            uri: songkick_performance.artist.uri
        };

        _performer.isHeadliner = songkick_performance.billing === 'headline' ? true : false;
        _performers.push(_performer);
    });

    return _performers;
};

exports.initializeStartModel = function initializeStartModel(songkickEvent){
    var _start = Object.create(Start);

    if (songkickEvent.start.date){
        _start.datetime = moment(songkickEvent.start.date);
        _start.displayDate = _start.datetime.format("MMM DD"); //Aug 12
        _start.displayDay = _start.datetime.format("dddd"); //Monday (use ddd for Mon)

    }

    if (songkickEvent.start.datetime) {
        var _date = new Date(songkickEvent.start.datetime);
        var hours = songkickEvent.start.time.substring(0, 2);
        var minutes = songkickEvent.start.time.substring(3, 5);
        _date.setHours(hours);
        _date.setMinutes(minutes);
        if (minutes === '00'){
            _start.displayTime = moment(_date).format("h a").toUpperCase(); //7PM
        }
        else{
            _start.displayTime = moment(_date).format("h:mm a").toUpperCase(); //7:30PM
        }
    }else{
        _start.displayTime = 'TBD'

    }

    return _start;
};

exports.initializeVenueModel = function initializeVenueModel(songkickEvent){
    var _venue = Object.create(Venue);
    _venue.songkickId = songkickEvent.venue.id;
    _venue.displayName = songkickEvent.venue.displayName;
    _venue.lat = songkickEvent.venue.lat;
    _venue.lng = songkickEvent.venue.lng;

    return _venue;
};
