var showmeService = require('../services/showmeService');
var spotifyController = require('../controllers/spotifyController.js');
var logger = require('../config/logger');


exports.events = function(req,res){
    var min_date = req.query.min_date ? encodeURIComponent(req.query.min_date): '';
    var max_date = req.query.max_date ? encodeURIComponent(req.query.max_date): '';
    var songkick_locationid = req.query.songkick_locationid ? encodeURIComponent(req.query.songkick_locationid) : '';
    var lat = req.query.lat ? encodeURIComponent(req.query.lat) : '';
    var lng = req.query.lng ? encodeURIComponent(req.query.lng) : '';
    var page = req.query.page ? encodeURIComponent(req.query.page) : 1;
    var access_token = req.session.client_credentials ? req.session.client_credentials.access_token : '';

    var parameters = {
      min_date,
      max_date,
      location_info: {
        songkick_locationid : songkick_locationid,
        lat : lat,
        lng : lng
      },
      page,
      access_token
    }

    var getEvents = function(parameters){
      showmeService.getEventsForALocation(parameters, function(err, Events){
        if (err){
            res.status(500).send(err)
        }else{
            logger.info('Returning event objects to client')
            res.status(200).json(Events);
        }
      });
    }

    // If access tokens fail, still show events returned from songkick
    try {
      if (access_token) {
         logger.debug('Access tokens received. Going to get events.')
         getEvents(parameters);
      } else {
        logger.info('No client access tokens detected. Going to fetch access tokens.')
        spotifyController.getClientAccessTokens(req, (err, request) => {
          if (!err){
            parameters.access_token = request.session.client_credentials.access_token;
            logger.info('Access tokens received. Going to get events.')
            getEvents(parameters);
          }else {
            logger.error(err)
          }
        })
      }
    } catch(e) {
      logger.error('Error getting access tokens. Going to get events without spotify access tokens.', e)
      getEvents(parameters)
    }
}
