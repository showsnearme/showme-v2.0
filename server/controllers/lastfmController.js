var request = require('request');
var logger = require('../config/logger');
var api_key = process.env.LAST_FM_KEY;
var baseURL = "http://ws.audioscrobbler.com/2.0/";
var headers = {
    'Content-Type': 'application/json'
};

exports.genres = function getGenres(req,res){
    var artist = encodeURIComponent(req.query.artist)
    var options = {
        url: baseURL + "?method=artist.gettoptags&artist=" + artist + "&api_key=" + api_key + "&format=json",
        headers: headers,
        json: true
    };

    logger.info('Going to get genre information from lastFM')
    request.get(options, function(err, response, body) {
        if (err){
            logger.error('Unable to get genre information from lastFM', err)
            res.status(500).send(err);

        } else if (body){
            if (body.error){
              logger.warn('Unable to get genre information from lastFM for: ' + artist, body.error)
            }
            logger.info('Succesfully received genre information from lastFM. Sending back to client.')
            logger.debug(body)
            res.status(200).json(body)
        }
    });
};

exports.similarartists = function getSimilarArtists(req,res){
    var artist = encodeURIComponent(req.query.artist)
    var options = {
        url: baseURL + "?method=artist.getsimilar&artist=" + artist + "&api_key=" + api_key + "&format=json",
        headers: headers,
        json: true
    };

    logger.info('Going to get similar artists from lastFM')
    request.get(options, function(err, response, body) {
      if (err){
         logger.error('Unable to get similar artists information from lastFM', err)
         res.status(500).send(err);

      } else if (body){
          if (body.error){
            logger.warn('Unable to get similar artists information from lastFM for: ' + artist, body.error)
            logger.debug(body)
          }
          logger.info('Succesfully received similar artist information from lastFM. Sending back to client.')
          logger.debug(body)
          res.status(200).json(body)
      }
    });
};
