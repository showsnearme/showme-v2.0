var logger = require('../config/logger');
var request = require('request');
var baseURL = "https://api.spotify.com/v1";
var config = require('../config/config');
var querystring = require('querystring');
var config = require('../config/config');
var spotifyService = require('../services/spotifyService');
var _ = require('underscore');
var colors = require('colors')

var baseUrl = "https://api.spotify.com/v1/";

/**************************
 * Methods with callbacks *
 **************************/

 exports.findArtistOnSpotify = function findArtistOnSpotify(access_token, songkick_artist_name, callback){
     var options = {
         url: baseURL + "/search?q=" + encodeURIComponent(songkick_artist_name) + "&type=artist&limit=3",
         headers: {
           'Content-Type': 'application/json',
           'Authorization': 'Bearer ' + access_token

         },
         json: true
     };

     request.get(options, function(error, response, body) {
         if (body && body.error){
           callback(body.error);
         } else if (body){
           callback(null, body);
         } else {
           callback('Unknown error getting artists on spotify')
         }
     });
 };

 exports.getTopTracks = function(access_token, artist_id, user_country, callback) {
     var options = {
         headers: {
           'Content-Type': 'application/json',
           'Authorization': 'Bearer ' + access_token

         },
         url:'https://api.spotify.com/v1/artists/' + artist_id + '/top-tracks?country=' + user_country,
         method: "GET",
         json: true
     };

     var desired_num_top_tracks = 3;

     request(options, function(err, response, body) {
         var tracks;

         if (body && body.error){
            callback(body.error);
         } else if (body && body.tracks){
            tracks = body.tracks.slice(0, desired_num_top_tracks);
            tracks = _.pluck(tracks, 'uri');

             if (tracks) {
                 callback(null, tracks)
             } else {
                 callback('Unknown error getting top tracks')
             }
         } else {
             logger.error('Unable to find tracks for spotify artist', body);
             callback('Unknown error getting artists on spotify')
         }
     })
 };

 exports.createPlaylist = function(access_token, user_id, playlist_name, playlist_description, callback) {
     var options = {
         headers: {
              "Content-Type": 'application/json',
              "Authorization": 'Bearer ' + access_token
         },
         url: baseUrl + 'users/' + user_id + '/playlists',
         json: {
             name: playlist_name,
             public: true,
             description: playlist_description
         },
         method: "POST"
     }

     logger.info('Going to create empty spotify playlist');
     request(options, function(err, response, body) {
         var playlist = {};

         if (err) {
             callback(err);
         } else if (body && body.error) {
             callback(body.error);
         } else if (body) {
                 playlist.uri = body.uri ? body.uri : '';
                 playlist.id = body.id ? body.id : '';
                 playlist.description = body.description ? body.description : '';
                 playlist.href = body.external_urls && body.external_urls.spotify ? body.external_urls.spotify : '';
                 callback(null, playlist)
         } else {
               callback('Could not create playlist id');
         }
     });
 };

 exports.addTracksToPlaylist = function(access_token, track_uris, user_id, playlist_id, callback) {
     var options = {
         method: "POST",
         url: baseUrl + 'users/' + user_id + '/playlists/' + playlist_id + '/tracks',
         headers: {
             "Content-Type": 'application/json',
             Authorization: 'Bearer ' + access_token
             },
         json: {
             uris: track_uris
         }
     }

     request(options, function(err, response, body) {
         if (err) {
             callback(err, false);
         } else if (body && body.error){
             callback(body.error, false);
         } else {
             callback(null, true);
         }
     });

 };


 /********************************************************
  * Methods that directly send back a Response to client *
  ********************************************************/

exports.getPlaylist = function getPlaylist(req,res){
    logger.info('Creating playlist')
    spotifyService.getPlaylistUri(req,res, (err, playlist) => {
        if (!err){
            logger.info('Succesfully created playlist and added tracks. Sending playlist URI to client.')
            res.status(200).send('success! Here is the link to your playlist: <a href="' + playlist.uri + '">' + playlist.uri + '</a> <div>' + req.session.playlist_info.description + '</div>');
        }else{
            logger.error(err)
            if (typeof err == 'string'){
                res.status(500).send('There was an error making the playlist: ' + err)
            }else{
                if (err.status && err.message){
                    res.status(err.status).send('There was an error making the playlist: ' + err.message)
                }else if(err.message){
                    res.status(500).send('There was an error making the playlist: ' + err.message)
                }else{
                    res.status(500).send('here was an error making the playlist')
                }
            }
        }
    })
};

exports.playlist = function playlist(req,res){
    req.session.playlist_info = req.body.data;

    if(req.session.user){
        spotifyService.getPlaylistUri(req,res, (err, playlist)=>{
            if (!err){
                res.status(200).send('success! Here is the link to your playlist: <a href="' + playlist.uri + '">' + playlist.uri + '</a> <div>' + req.session.playlist_info.description + '</div>');
            }else{
                if (err.message === 'The access token expired'){
                    logger.info('Tokens expired.. going to reauthorize..');
                    spotifyService.authorizeUser(req,res);
                }else{
                    if (typeof err == 'string'){
                        res.status(500).send('There was an error making the playlist' + err)
                    }else{
                        if (err.status && err.message){
                            res.status(err.status).send('There was an error making the playlist: ' + err.message)
                        }else if(err.message){
                            res.status(500).send('There was an error making the playlist: ' + err.message)
                        }else{
                            res.status(500).send('Unknown error making the playlist')
                        }
                    }
                }
            }
        });
    }else{
        spotifyService.authorizeUser(req,res)
    }
};

exports.getAuthTokens = function(req, res) {
    var client_id = config.spotify.client_id,
        client_secret = config.spotify.client_secret,
        redirect_uri =  config.spotify.redirect_uri;  /*/api/spotify/token*/

    var code = req.query.code || null;
    var state = req.query.state || null;
    var storedState = req.session ? req.session.state : null;

    if (state === null || state !== storedState) {
        res.redirect('/#' +
            querystring.stringify({
                error: 'state_mismatch'
            }));
    } else {
        req.session.state = null;
        var authOptions = {
            url: 'https://accounts.spotify.com/api/token',
            form: {
                code: code,
                redirect_uri: redirect_uri,
                grant_type: 'authorization_code'
            },
            headers: {
                'Authorization': 'Basic ' + (new Buffer(client_id + ':' + client_secret).toString('base64'))
            },
            json: true
        };

        request.post(authOptions, function(error, response, body) {
            if (!error && response.statusCode === 200) {

                var access_token = body.access_token;
                var refresh_token = body.refresh_token;
                var options = {
                    url: 'https://api.spotify.com/v1/me',
                    headers: { 'Authorization': 'Bearer ' + body.access_token },
                    json: true
                };

                // use the access token to access the Spotify Web API
                request.get(options, function(error, response, body) {
                    req.session.user = body;
                    req.session.user.access_token = access_token;
                    req.session.user.refresh_token = refresh_token;

                    res.redirect('/api/spotify/playlist');
                });

            } else {
                res.redirect('/#' +
                    querystring.stringify({
                        error: 'invalid_token'
                    })
                );
            }
        });
    }
};

exports.getClientAccessTokens = function(req, callback) {
  var client_id = config.spotify.client_id,
      client_secret = config.spotify.client_secret;

  var header = 'Basic ' + (new Buffer(config.client_id + ':' + client_secret).toString('base64'))


  var authOptions = {
      url: 'https://accounts.spotify.com/api/token',
      form: {
          grant_type: 'client_credentials'
      },
      headers: {
          'Authorization': 'Basic ' + (new Buffer(client_id + ':' + client_secret).toString('base64'))
      },
      json: true
  };

  request.post(authOptions, function(err, response, body) {
    if (err) {
        callback(err);
    }else if (body.error) {
        callback(body.error);
    } else {
        const session = Object.assign({}, req.session, {client_credentials: body});
        req.session = session;
        callback(null, req)
    }
  })
}
