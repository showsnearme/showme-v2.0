var request = require('request');
var songkickService = require('../services/songkickService');
var logger = require('../config/logger');
var key = process.env.SONGKICK_KEY;
var baseURL = "https://api.songkick.com/api/3.0";
var headers = {
    'Content-Type': 'application/json'
};

exports.location = function getLocation(req, res){
    var options = {
        url: baseURL + "/search/locations.json?query=" + encodeURIComponent(req.query.location) + "&apikey=" + key,
        headers: headers,
        json: true
    };

    logger.info('Searching for songkick location')
    request.get(options, function(err, response, body) {

        if (err) {
            logger.error('Unable to get songkick locations', err);
            res.status(500).send(err);
        } else if (body.resultsPage && body.resultsPage.status === 'ok') {
            var locations = {};
            locations = songkickService.buildLocationsArray(body.resultsPage.results.location);
            logger.error('Succesfully received songkick locations. Sending to client.');
            logger.debug(body)
            res.status(200).json(locations);
        } else {
            const message = body.resultsPage && body.resultsPage.status === 'error' ? body.resultsPage.error.message : 'Unable to get songkick location';
            logger.error('Unable to get songkick locations', message);
            logger.debug(body)
            res.status(500).send(messgae);
        }
    });
};

exports.events = function getEvent(min_date,max_date, songkick_locationid, page, callback){
    var options = {
        url: baseURL + "/events.json?apikey=" + key + "&location=sk:" + songkick_locationid + "&min_date=" + min_date + "&max_date=" + max_date + "&page=" + page,
        headers: headers,
        json: true
    };

    logger.info('Going to get songkick events by songkick location id')
    request.get(options, function(err, response, body) {
        if (err){
            callback(err);
        } else if (body && body.resultsPage.status === 'ok'){
            callback(null, body);
        } else {
            const message = body.resultsPage && body.resultsPage.status === 'error' ? body.resultsPage.error.message : 'Unable to get songkick events';
            callback(message);
        }
    });
};

exports.getEventsByLatitudeAndLongitude = function getEvent(min_date, max_date, lat, lng, page, callback){
    var options = {
        url: baseURL + "/events.json?apikey=" + key + "&location=geo:" + lat + "," + lng + "&min_date=" + min_date + "&max_date=" + max_date + "&page=" + page,
        headers: headers,
        json: true
    };

    logger.info('Going to get songkick events by latitude and longitude')
    request.get(options, function(err, response, body) {
        if (err){
            callback(err);
        } else if (body && body.resultsPage.status === 'ok'){
            callback(null, body);
        } else {
            const message = body.resultsPage && body.resultsPage.status === 'error' ? body.resultsPage.error.message : 'Unable to get songkick events by lat/lng';
            callback(message);
        }
    });
};

exports.venue = function getVenueDetails(req, res){
    var options = {
        url: baseURL + "/venues/" + encodeURIComponent(req.query.songkick_venueid) + ".json?apikey=" + key,
        headers: headers,
        json: true
    };

    logger.info('Going to get venue information from songkick')
    request.get(options, function(err, response, body) {
        if (err) {
            logger.error('Unable to get venue details', err)
            res.status(500).send(err);
        } else if (body.resultsPage && body.resultsPage.status === 'ok') {
            var locations = {};
            logger.info('Succesfully received venue information from songkick. Sending back to client.')
            logger.debug(body)
            res.status(200).json(body);
        } else {
            const message = body.resultsPage && body.resultsPage.status === 'error' ? body.resultsPage.error.message : 'Unable to get venue details';
            logger.error('Unable to get venue details', message);
            logger.debug(body)
            res.status(500).send(messgae);
        }
    });
};
