var spotifyController = require('../controllers/spotifyController');
var songkickController = require('../controllers/songkickController')
var showmeController = require('../controllers/showmeController')
var lastfmController = require('../controllers/lastfmController')

module.exports = function(app) {

    app.get('/api/songkick/location',  songkickController.location);

    app.get('/api/showme/events',  showmeController.events);

    app.get('/api/showme/venue',  songkickController.venue);

    app.get('/api/lastfm/similarartists',  lastfmController.similarartists);

    app.get('/api/lastfm/genres',  lastfmController.genres);

    app.get('/api/spotify/playlist', spotifyController.getPlaylist);

    app.post('/api/spotify/playlist', spotifyController.playlist);
    //TODO: app.post('/api/spotify/playlist', spotifyController.getPlaylist);

    app.get('/api/spotify/token', spotifyController.getAuthTokens)

    app.get('/api/spotify/clientCredentials', spotifyController.getClientAccessTokens)
}
