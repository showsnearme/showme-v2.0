var mongoose = require('mongoose');
var logger = require('./logger');
var config = require('./config');
var colors = require('colors');

module.exports = function() {
    if (process.env.NODE_ENV !== 'test') {
        mongoose.set('debug', config.db.debugMode)
    }
    var options = {
        server: {
            socketOptions: {
                keepAlive: 300000,
                connectTimeoutMS: 10000
            }
        }
    };
    logger.info("connecting to " + config.db.url + " mongo instance");

    mongoose.connect(config.db.url, options, function(err) {

        if (err) {
            logger.error('YOU HAVE NOT PROPERLY AUTHENTICATED TO MONGOOSE'.red, err);
            // throw err };
        }
    });

    var db = mongoose.connection;
    db.once('open', function callback() {
        logger.info('showme db connected'.green);
    });
};
