var colors = require('colors');


var logger = require('tracer')
		.colorConsole(
				{
          level: 'info',
          filters : {
      			//log : colors.black,
      			trace : colors.gray,
      			debug : colors.white,
      			info : colors.blue,
      			warn : colors.yellow,
      			error : [ colors.red, colors.bold ]
      		},
					format : [
					          "{{timestamp}} <{{title}}> {{message}} (in {{file}}:{{line}})", //default format
					          {
					        	  error : "{{timestamp}} <{{title}}> {{message}} (in {{file}}:{{line}})\nCall Stack:\n{{stack}}" // error format
					          }
					],
					dateformat : "HH:MM:ss.L",
					preprocess :  function(data){
						data.title = data.title.toUpperCase();
					}
				});

module.exports = logger;
