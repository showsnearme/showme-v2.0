var express = require('express'); // Express web server framework
var cookieSession = require('cookie-session');
var config = require('./config');
var bodyParser = require('body-parser');

module.exports = function (app){
  var sess = {
    key: 'session',
    secret: config.session_cookie_secret,
    cookie: {}
  }
//TODO: need to see the cookie.secure value to true (https only) for production -- https://github.com/expressjs/session

  app.use(express.static( config.rootPath + '/public/dist'))
     .use(cookieSession(sess))

  app.use(bodyParser.urlencoded({extended:true}));
  app.use(bodyParser.json());
}
