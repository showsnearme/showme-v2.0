var moment = require('moment');

exports.vendorConfiguration = function vendorConfiguration(){
    moment.updateLocale('en', {
        monthsShort : [
            "Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"
        ]
    });
}
