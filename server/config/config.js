/**
 * Template taken from : https://github.com/pblair12/mosaic-groups/blob/master/server/config/config.js
 */
var logger = require('./logger');

var path = require('path');
var fs = require('fs');
var colors = require('colors');

//globals
var rootPath = path.normalize(__dirname + '/../../');
var env = process.env.NODE_ENV = process.env.NODE_ENV || 'local';
var localport = 4000;

logger.info('running environment ', env.yellow);

var envs = {
    local: {
        env: env,
        domain: 'localhost',
        db: {
            url: 'mongodb://showme:' + process.env.MONGO_PASSWORD + '@dbh63.mlab.com:27637/showme-dev',
            debugMode: true
        },
        rootPath: rootPath,
        http: {
            port: process.env.PORT || localport
        },
        spotify: {
          redirect_uri: 'http://localhost:' + localport + '/api/spotify/token',
          client_id : process.env.CLIENT_ID,
          client_secret : process.env.CLIENT_SECRET
        },
        session_cookie_secret: process.env.SESSION_COOKIE_SECRET
        /*https: {
            port: process.env.SSLPORT || 3031,
            options: {
                key: fs.readFileSync('server/certs/server.key'),
                cert: fs.readFileSync('server/certs/server.crt')
            }
        },*/
    },

    developmentSara: {
        env: env,
        domain: 'showsnearme-sara.herokuapp.com',
        db: {
            url: 'mongodb://showme:' + process.env.MONGO_PASSWORD + '@dbh63.mlab.com:27637/showme-dev',
            debugMode: true
        },
        rootPath: rootPath,
        http: {
            port: process.env.PORT || 80
        },
        spotify: {
            redirect_uri: 'http://showsnearme-sara.herokuapp.com/api/spotify/token',
            client_id : process.env.CLIENT_ID,
            client_secret : process.env.CLIENT_SECRET,
        },
        session_cookie_secret: process.env.SESSION_COOKIE_SECRET || 'something123'
        /*https: {
         port: process.env.SSLPORT || 3031,
         options: {
         key: fs.readFileSync('server/certs/server.key'),
         cert: fs.readFileSync('server/certs/server.crt')
         }
         },*/
    },

    staging: {
        env: env,
        domain: 'showmethemusic-staging.herokuapp.com',
        db: {
            url: 'mongodb://showme:' + process.env.MONGO_PASSWORD + '@dbh63.mlab.com:27637/showme-dev',
            debugMode: true
        },
        rootPath: rootPath,
        http: {
            port: process.env.PORT || 80
        },
        spotify: {
          redirect_uri: 'http://showmethemusic-staging.herokuapp.com/api/spotify/token',
          client_id : process.env.CLIENT_ID,
          client_secret : process.env.CLIENT_SECRET,
        },
        session_cookie_secret: process.env.SESSION_COOKIE_SECRET
        /*https: {
            port: process.env.SSLPORT || 3031,
            options: {
                key: fs.readFileSync('server/certs/server.key'),
                cert: fs.readFileSync('server/certs/server.crt')
            }
        },*/
    },

    production: {
        env: env,
        domain: 'showmethemusic.co',
        db: {
            url: 'mongodb://showme:' + process.env.MONGO_PASSWORD + '@ds049161.mlab.com:49161/showme',
            debugMode: false
        },
        rootPath: rootPath,
        http: {
            port: process.env.PORT || 80
        },
        spotify: {
          redirect_uri: 'http://showmethemusic.co/api/spotify/token',
          client_id : process.env.CLIENT_ID,
          client_secret : process.env.CLIENT_SECRET,
        },
        session_cookie_secret: process.env.SESSION_COOKIE_SECRET
        /*https: {
            port: process.env.SSLPORT || 443,
            options: {
                key: fs.readFileSync('server/certs/server.key'),
                cert: fs.readFileSync('server/certs/server.crt')
            }
        }*/
    }
}

envs.test = envs.development;

module.exports = envs[env];
