module.exports = {
      country: "",
      display_name: "",
      email: "",
      external_urls: {
          spotify: ''
      },
      followers: {
          href: null,
          total: 14
      },
      href: '5',
      id: '123456789',
      images:
       [ { height: null,
           url: '',
           width: null }
       ],
      product: 'premium',
      type: 'user',
      uri: 'spotify:user:123456789',
      access_token: '',
      refresh_token: ''
}
