var Performer = require('./Performer');
var Start = require('./Start');
var Venue = require('./Venue');

module.exports = {
    performers: [Performer],
    start: Start,
    venue: Venue,
    genres: [""],
    id: ''
}

//http://api.songkick.com/api/3.0/events.json?apikey={your_api_key}&location=sk:1467&min_date=2009-10-01&max_date=2009-10-30
