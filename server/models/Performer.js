module.exports = {
    "displayName": "",
    "spotify": {
        artist_id: "",
        displayName: ""
    },
    "songkick": {
        displayName: "",
        artist_id: "",
        uri: ""
    },
    "images": [{
        "width": 0,
        "height": 0,
        "uri": ""
    }],
    "genres": [],
    "isHeadliner": true,
    "popularity": 0,
    "isAvailableOnSpotify": false
}