module.exports = {
  "songkickId": 0,
   "displayName":"",
   "city":{
       "uri":"",
   		"displayName":"",
        "country":{
            "displayName":""
        },
        "id":0
    },
   "metroArea":{
       "uri":"",
		"displayName":"",
        "country":{
            "displayName":""
        },
        "id":0
    },
   "uri":"",
   "street":"",
   "zip":"",
   "lat":0.0,
   "lng":0.0,
   "phone":"",
   "website":"",
   "capacity":0,
   "description": ""
}

//http://api.songkick.com/api/3.0/venues/{venue_id}.json?apikey={your_api_key}
