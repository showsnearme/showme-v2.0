export function momentPropType(props, propName, componentName){
    componentName = componentName || 'Unknown';

    if (!props[propName]._isAMomentObject){
        return new Error('Invalid prop `' + propName + '` supplied to ' + componentName + '. Expecting a Moment object')
    }
}
