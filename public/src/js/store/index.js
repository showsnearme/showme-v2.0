import moment from 'moment';
import deepFreeze from 'deep-freeze'
import Immutable from 'immutable'

import { UPDATE_EVENTS } from './actions';
import { CLEAR_EVENTS } from './actions';
import { SHOW_SEARCH_RESULTS_SPINNER } from './actions';
import { INCREMENT_PAGE_DATA } from './actions';
import { ADD_SONGKICK_EVENTS_TO_VIEW } from './actions';
import { TOGGLE_MENU_OVERLAY } from './actions';

import { CLOSE_EVENT_DETAILS_OVERLAY } from './actions';
import { OPEN_EVENT_DETAILS_OVERLAY } from './actions';
import { TOGGLE_EVENT_ON_PLAYLIST } from './actions';

import { TOGGLE_NAME_PLAYLIST_OVERLAY } from './actions';

import { UPDATE_STATE } from './actions';
import { NO_EVENTS_FOUND } from './actions';

const DEFAULT_STATE = {
    location: {
        displayName: 'New York, NY',
        songkick_locationid: 7644,
        lat: 40.8399,
        lng: -73.9422
    },
    minDate: moment(),
    maxDate: moment(),
    eventsMetaData: {
        numberOfResults: 0,
        numberOfPages: 1,
        currentPage: 1
    },
    events: [],
    eventsLoading: false,
    eventDetailsOverlay: {
        selectedEvent: {
            start: {},
            performers: [],
            venue: {},
            id: 0,
            isSelected: false
        },
        showEventDetailsOverlay: false,
        similarArtists: [''],
        genres: [''],
        venue: {
            displayName: '',
            lat: 0.000,
            lng: 0.000,
            songkickId: 0
        }
    },
    numSelectedEvts: 0,
    showMenu: false,
    showNamePlaylist: false
};


const showSearchResultsSpinner = (state, action) => {
    return Object.assign({}, state, {eventsLoading: true});
}

const clearEvents = (state, action) => {
    return Object.assign({}, state, {events: []});
}

const addSongkickEventsToView = (state, action) => {
    let events, metaData;

    if (action.results){
        metaData = action.results.metaData

        if (action.appendToExistingResults)
            events = [...state.events, ...action.results.events]
        else
            events = action.results.events;

    }else{
        events = [];
        metaData = {
            numberOfResults: 0,
            numberOfPages: 1,
            currentPage: 1
        };
    }

    const newState = {
        events: events,
        eventsMetaData: metaData,
        eventsLoading: false,
    }

    //localStorage.setItem("events", events)

    return Object.assign({}, state, newState);
};

const incrementPage = (state, action) => {
    const newState = {
        eventsMetaData: {
            currentPage: state.eventsMetaData.currentPage + 1
        }
    }

    return Object.assign({}, state, newState);
}

const updateState = (state, action) => {
    return Object.assign({}, state, action.newState);
}

const closeEventDetailsOverlay = (state, action) => {
    const eventDetailsOverlay = Object.assign({}, state.eventDetailsOverlay, {showEventDetailsOverlay: false});
    return Object.assign({}, state, {eventDetailsOverlay: eventDetailsOverlay});
}


const toggleEventOnPlaylist = (state, action) => {
    let numSelectedEvts;
    const oldState = Immutable.Map(state);

    //toggle event on playlist
    const selectedEvent = Immutable.Map(action.selectedEvent).set('isSelected', !action.selectedEvent.isSelected)

    const indexOfSelectedEvent = state.events.findIndex( (evt)=>{
        return evt.id === action.selectedEvent.id
    });

    //replace updated event on array
    const updatedEventsList = Immutable.List(state.events).set(indexOfSelectedEvent, selectedEvent)

    //decrement || incremement selected evts counter
    if (selectedEvent.get('isSelected'))
        numSelectedEvts = state.numSelectedEvts + 1;
    else
        numSelectedEvts = state.numSelectedEvts - 1;

    return oldState.set('events', updatedEventsList).set('numSelectedEvts', numSelectedEvts).toJS();
}

const toggleMenuOverlay = (state, action) => {
    return Object.assign({}, state, {showMenu: !state.showMenu});
}

const toggleNamePlaylistOverlay = (state, action) => {
    return Object.assign({}, state, {showNamePlaylist: !state.showNamePlaylist});
}

const rootReducer = (state = DEFAULT_STATE, action) => {
    switch (action.type){
        case ADD_SONGKICK_EVENTS_TO_VIEW:
            return addSongkickEventsToView(state, action)
            break;
        case UPDATE_STATE:
            return updateState(state, action)
            break;
        case NO_EVENTS_FOUND:
            return updateState(state, action)
            break;
        case INCREMENT_PAGE_DATA:
            return incrementPage(state, action)
            break;
        case SHOW_SEARCH_RESULTS_SPINNER:
            return showSearchResultsSpinner(state, action)
            break;
        case CLEAR_EVENTS:
            return clearEvents(state, action)
            break;
        case CLOSE_EVENT_DETAILS_OVERLAY:
            return closeEventDetailsOverlay(state, action)
            break;
        case TOGGLE_EVENT_ON_PLAYLIST:
            return toggleEventOnPlaylist(state, action)
            break;
        case TOGGLE_MENU_OVERLAY:
            return toggleMenuOverlay(state, action)
            break;
        case TOGGLE_NAME_PLAYLIST_OVERLAY:
            return toggleNamePlaylistOverlay(state, action)
        default:
            return state
    }
};

export default rootReducer
