import { UPDATE_EVENTS } from './actions';
import { SHOW_SEARCH_RESULTS_SPINNER } from './actions';
import { INCREMENT_PAGE_DATA } from './actions';
import { ADD_SONGKICK_EVENTS_TO_VIEW } from './actions';
import { TOGGLE_MENU_OVERLAY } from './actions';
import { CLEAR_EVENTS } from './actions';


import { CLOSE_EVENT_DETAILS_OVERLAY } from './actions';
import { OPEN_EVENT_DETAILS_OVERLAY } from './actions';
import { TOGGLE_EVENT_ON_PLAYLIST } from './actions';

import { TOGGLE_NAME_PLAYLIST_OVERLAY } from './actions';

import { UPDATE_STATE } from './actions';
import { NO_EVENTS_FOUND } from './actions';

import songkick from '../services/songkick'
import lastfm from '../services/lastfm'
import async from 'async';


/**********
* HELPERS *
***********/
function updateState (reason, newState) {
    return {
        type: UPDATE_STATE,
        reason,
        newState
    }
}

function noEventsFound (newState) {
    return {
        type: NO_EVENTS_FOUND,
        newState
    }
}

/************************
 * NAME PLAYLIST OVERLAY
 ************************/
export function toggleNamePlaylistModal(){
    return {
        type: TOGGLE_NAME_PLAYLIST_OVERLAY
    }
}


/************************
* EVENT DETAILS OVERLAY *
*************************/
export function closeEventDetailsOverlay (){
    return{
        type: CLOSE_EVENT_DETAILS_OVERLAY
    }
}

export function getEventDetails (evt){
    return (dispatch, getState) =>{
        //Need to bind content here. See https://github.com/caolan/async/issues/431
        const task1 = lastfm.getSimilarArtistsTo.bind(null, evt.performers[0].displayName);
        const task2 = lastfm.getGenresOf.bind(null, evt.performers[0].displayName);
        const task3 = songkick.getVenueDetails.bind(null, evt.venue.songkickId);

        var this_component = this;
        async.parallel([task1, task2, task3], function(err, results){
            let similarArtists, genres, venue = null;

            try{
                if (results[0].similarartists.error){
                    similarArtists = 'No related artists available.'
                }else{
                    similarArtists = results[0].similarartists.artist.slice(0,3)

                }
            }catch(e){
                similarArtists = 'No related artists available';
                console.log('No related artists available', results[0])
            }

            try{
                if (results[1].toptags && results[1].toptags.tag){
                    genres = results[1].toptags.tag.slice(0,5);
                }else{
                    genres = 'No genres available'
                }
            }catch(e){
                genres = 'No genres available'
            }

            try{
                venue = results[2].resultsPage.results.venue;
            }catch(e){
                venue = 'venue not available'
            }

            const newState = {
                eventDetailsOverlay: {
                    showEventDetailsOverlay: true,
                    selectedEvent: evt,
                    similarArtists,
                    genres,
                    venue
                }
            }

            dispatch(updateState('open event details', newState));
        })
    }
}

export function toggleEventOnPlaylist (selectedEvent){
    return{
        type: TOGGLE_EVENT_ON_PLAYLIST,
        selectedEvent
    }
}

export function clearEvents (){
    return{
        type: CLEAR_EVENTS
    }
}

/*******************
* EVENTS DASHBOARD *
********************/

export function toggleMenuOverlay(){
    return {
        type: TOGGLE_MENU_OVERLAY
    }
}

export function updateEvents (data) {
    return (dispatch, getState) => {
        const parameter = data ? Object.keys(data)[0] : undefined, oldState = getState();
        let newState = {}, reason;

        switch (parameter){
            case 'min_date':
                //min date must be less than current max date
                const newMinDate = data.min_date;

                if (newMinDate.diff(oldState.maxDate) < 0) {
                    newState.minDate = newMinDate

                }else{
                    newState.minDate = newMinDate
                    newState.maxDate = newMinDate
                }
                reason = 'min_date change'
                break;
            case 'max_date':
                //max date must be larger than current min date
                const newMaxDate = data.max_date;

                if (newMaxDate.diff(oldState.minDate)  > 0) {
                    newState.maxDate = newMaxDate
                }else{
                    newState.minDate = newMaxDate,
                    newState.maxDate = newMaxDate
                }
                reason = 'max_date change'
                break;
            case 'location':
                newState.location = {
                    songkick_locationid: data.location.id,
                    displayName: data.location.displayName,
                    lat: data.location.lat,
                    lng: data.location.lng
                };
                reason = 'location change'
                break;
            case 'page':
                const eventsMetaData = Object.assign({}, oldState.eventsMetaData, {currentPage: data.page})

                newState.eventsMetaData = eventsMetaData;
                reason = 'additional page of data requested'
                break;
            default:
                break;
        }

        if (reason)
            dispatch(updateState(reason, newState));

        const updatedState = Object.assign({}, getState(), newState);

        const min_date = updatedState.minDate.format('YYYY-MM-DD');
        const max_date = updatedState.maxDate.format('YYYY-MM-DD');
        const songkick_locationid = updatedState.location.songkick_locationid;
        const page = updatedState.eventsMetaData.currentPage;

        songkick.getEvents(min_date, max_date, songkick_locationid, page, (err, results) => {
            if (!err && typeof results === 'object'){
                if (parameter === 'page'){
                    const appendToExistingResults = true;
                    dispatch(addSongkickEventsToView(results, appendToExistingResults))
                }else{
                    dispatch(addSongkickEventsToView(results))
                }
            }else {
                dispatch(noEventsFound({eventsLoading: false}))
                console.error('Error getting songkick events', err)
            }
        })
    }
}

export function getEventsFromLatitudeAndLongitude (latitude, longitude) {
    return function(dispatch, getState){
        const state = getState();
        const min_date = state.minDate.format('YYYY-MM-DD');
        const max_date = state.maxDate.format('YYYY-MM-DD');

        songkick.getEventByLatitudeAndLongitude(min_date, max_date, latitude, longitude, (err, results) => {
            if (!err && typeof results === 'object'){
                dispatch(addSongkickEventsToView(results))
            }else {
                dispatch(addSongkickEventsToView(null))
                console.error(`Error getting songkick events: ${err} ${results}`)
            }
        })
    }
}

export function addSongkickEventsToView (results, appendToExistingResults) {
    return {
        type: ADD_SONGKICK_EVENTS_TO_VIEW,
        results,
        appendToExistingResults
    }
}

export function showSearchResultsSpinner () {
    return {
        type: SHOW_SEARCH_RESULTS_SPINNER,
    }
}

export function incrementPage () {
    return {
        type: INCREMENT_PAGE_DATA,
    }
}
