/**********
* HELPERS *
***********/
export const UPDATE_STATE = 'UPDATE_STATE';
export const NO_EVENTS_FOUND = 'NO_EVENTS_FOUND';

/*******************
* EVENTS DASHBOARD *
********************/
export const UPDATE_EVENTS = 'UPDATE_EVENTS';
export const SHOW_SEARCH_RESULTS_SPINNER = 'SHOW_SEARCH_RESULTS_SPINNER';
export const INCREMENT_PAGE_DATA = 'INCREMENT_PAGE_DATA';
export const ADD_SONGKICK_EVENTS_TO_VIEW = 'ADD_SONGKICK_EVENTS_TO_VIEW';
export const TOGGLE_MENU_OVERLAY = 'TOGGLE_MENU_OVERLAY';
export const CLEAR_EVENTS = 'CLEAR_EVENTS';

/************************
* EVENT DETAILS OVERLAY *
*************************/
export const CLOSE_EVENT_DETAILS_OVERLAY = 'CLOSE_EVENT_DETAILS_OVERLAY';
export const OPEN_EVENT_DETAILS_OVERLAY = 'OPEN_EVENT_DETAILS_OVERLAY';
export const TOGGLE_EVENT_ON_PLAYLIST = 'TOGGLE_EVENT_ON_PLAYLIST';

/***************************
* LISTEN TO ARTISTS FOOTER *
****************************/
export const TOGGLE_NAME_PLAYLIST_OVERLAY = 'TOGGLE_NAME_PLAYLIST_OVERLAY';
