import React from 'react';
import PlayIcon from '../SVG/PlayIcon';
import CheckmarkIcon from '../SVG/CheckmarkIcon';
import { toggleNamePlaylistModal } from '../../store/actionCreators';
import {connect} from 'react-redux';

class ListenToArtistsFooter extends React.Component {
    constructor(props){
        super(props)
        this.promptUserToNamePlaylist = this.promptUserToNamePlaylist.bind(this);
    }

    promptUserToNamePlaylist(){
        if (this.props.numSelectedEvts > 0){
            this.props.dispatch(toggleNamePlaylistModal())
        }
    }

    render() {
        return(
            <footer className="listen-to-artists">
                <div className= {`listen-to-selected-artists-container ${this.props.numSelectedEvts > 0 ? '' : 'disabled'} `}
                    onClick={this.promptUserToNamePlaylist}>
                    <PlayIcon />
                    <div className="play-button label">listen to selected events</div>
                    <div className="tooltip hidden">please check an event</div>
                </div>
            </footer>
        )
    }
}

/**********************
  PROP TYPE VALIDATIORS
  **********************/
const { array, number, func } = React.PropTypes;

const mapStateToProps = (state) => {
    return {
        events: state.events,
        numSelectedEvts: state.numSelectedEvts
    }
}

ListenToArtistsFooter.propTypes = {
        events: array,
        numSelectedEvts: number,
        dispatch: func
    }

export default connect(mapStateToProps)(ListenToArtistsFooter);
