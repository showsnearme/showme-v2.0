import React from 'react';
import Error from './Error';
import Spinner from '../../SVG/Spinner';
import EventCard from './EventCard';
import { connect } from 'react-redux';

class EventCardsContainer extends React.Component {
    constructor(props) {
      super(props);
    }

    render() {
        var cards, showSpinnerOnPagePull = null;

        if (this.props.events && this.props.events[0] && this.props.events[0].performers.length > 0 ){
            if (this.props.eventsLoading)
                showSpinnerOnPagePull = <Spinner color="#C2AF89"/>;

            cards = this.props.events.map( (evt, index) => {
                return(
                    <EventCard evt={evt} key={evt.id}/>
                )
            })

        }else{
            if (this.props.eventsLoading)
                cards = <Spinner color="#C2AF89"/>;
            else
                cards = <Error message="Cannot find events for this location."/>;
        }

        return(
            <section className="search-results">
                {cards}
                {showSpinnerOnPagePull}
            </section>
        )
    }
}

const { array, bool } = React.PropTypes;

EventCardsContainer.PropTypes = {
    events: array,
    eventsLoading: bool
}

const mapStateToProps = (state) => {
    return{
        events: state.events,
        eventsLoading: state.eventsLoading
    }
}

export default connect(mapStateToProps)(EventCardsContainer);
