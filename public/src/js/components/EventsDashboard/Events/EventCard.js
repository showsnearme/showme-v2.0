import React from 'react';
import CheckmarkIcon from '../../SVG/CheckmarkIcon';
import { connect} from 'react-redux';
import { getEventDetails, toggleEventOnPlaylist } from '../../../store/actionCreators'
import _ from 'underscore'

class EventCard extends React.Component {
    constructor(props){
        super(props)
        this.getEventDetails = this.getEventDetails.bind(this);
        this.toggleEventOnPlaylist = this.toggleEventOnPlaylist.bind(this);
    }

    toggleEventOnPlaylist(){
        this.props.dispatch(toggleEventOnPlaylist(this.props.evt))
    }

    getEventDetails(){
        this.props.dispatch(getEventDetails(this.props.evt))
    }

    render() {
        let image_url, inlineStyles, tooltipText, isAvailableOnSpotify = null;
        const this_component = this;

        if (this.props.evt.performers[0].images[0] && this.props.evt.performers[0].images[0].url){
            image_url = this.props.evt.performers[0].images[0].url;
            inlineStyles = {
                "backgroundImage": "url(" +image_url + ")"
            }
        }else{
            inlineStyles = {
                "backgroundColor": 'rgba(109, 126, 117, 0.85)'
            }
        }

        this.props.evt.performers.some( (performer) => {
            if (performer.isAvailableOnSpotify){
                isAvailableOnSpotify = 'available-on-spotify';
                tooltipText = 'add to playlist'
            }else{
                isAvailableOnSpotify = 'not-available-on-spotify';
                tooltipText = 'artist not available on spotify'
            }
        })

        return(
            <div className="event-info-card-container grid-mobile-12 grid-tablet-6 grid-desktop-4 grid-large-desktop-3">
                   <div className="event-info-card">
                       <section className="event-details">
                           <div className="event-details-container"  onClick={this.getEventDetails}>
                               <div className="artist-name detail">{this.props.evt.performers[0].displayName}</div>
                               <div className="venue detail">{this.props.evt.venue.displayName}</div>
                               <div className="showdate detail">{this.props.evt.start.displayDate} @</div>
                               <div className="showtime detail">{this.props.evt.start.displayTime}</div>
                           </div>
                           <div style={inlineStyles} onClick={this.getEventDetails} className="background-image-container"></div>
                       </section>

                       <section className="actions">
                           <div onClick={this.toggleEventOnPlaylist}
                               className= {`add-to-playlist-checkmark ${isAvailableOnSpotify} ${(this.props.evt.isSelected ? 'selected' : 'unselected')}` }>

                               <div className="checkmark-container">
                                   <CheckmarkIcon />
                                   <div className="tooltip-text">{tooltipText}</div>
                               </div>
                           </div>
                           <div className="open-overlay" onClick={this.getEventDetails}>
                               <div className="vertical-text-container">
                                   <span className="see-more-details-text">DETAILS</span>
                               </div>
                           </div>
                       </section>
                   </div>
               </div>
        )
    }
}

const { object, shape, func, array, arrayOf, number, string, bool } = React.PropTypes;

EventCard.PropTypes = {
    evt: shape({
        performers: arrayOf(shape({
                displayName: string,
                isAvailableOnSpotify: bool,
                isHeadliner: bool,
                popularity: number,
                songkick: {
                    artist_id: number,
                    displayName: string,
                    uri: string
                },
                spotify: {
                    artist_id: string,
                    displayName: string
                },
                genres: arrayOf(string),
                images: [{
                    height: number,
                    url: string,
                    width: number
                }]
        })),
        start: {
            displayDay: string,
            displayTime: string
        },
        venue: shape({
            displayName: string,
            lat: number,
            lng: number,
            songkickId: number
        }),
        id: number,
        isSelected: bool
    }),
    dispatch: func
}

export default connect()(EventCard);
