import React from 'react'
import MenuIcon from '../SVG/MenuIcon';
import ArtistIcon from '../SVG/ArtistIcon';
import CalendarIcon from '../SVG/CalendarIcon';
import MicrophoneIcon from '../SVG/MicrophoneIcon';
import VenueIcon from '../SVG/VenueIcon';
import { toggleMenuOverlay } from '../../store/actionCreators'
import { connect } from 'react-redux'


class EventsDashboardHeader extends React.Component {
    constructor(props){
        super(props)
        this.toggleMenuOverlay = this.toggleMenuOverlay.bind(this);
    }

    toggleMenuOverlay(){
        this.props.dispatch(toggleMenuOverlay())
    }

    render() {
        return(
            <header className="search-header-container">
                <div className="menu-container" onClick={this.toggleMenuOverlay}>
                    <MenuIcon />
                    <span className="number-of-results">{this.props.numberOfResults} events</span>
                </div>
                <div className="sort-options-container">
                    <label className="subtitle">sort by:</label>
                    <ul className="sort-options">
                        <li className="sort-option artist selected">
                            <ArtistIcon/>
                            <span className="sort-option-label">artist</span>
                        </li>
                        <li className="sort-option calendar coming-soon">
                            <CalendarIcon/>
                            <span className="sort-option-label">date</span>
                        </li>
                        <li className="sort-option venue coming-soon">
                            <VenueIcon/>
                            <span className="sort-option-label">venue</span>
                        </li>
                        <li className="sort-option genre coming-soon">
                            <MicrophoneIcon/>
                            <span className="sort-option-label">genre</span>
                        </li>
                    </ul>
                </div>
            </header>
        )
    }
}

const { func, number } = React.PropTypes;

const mapStateToProps = (state) => {
    return {
        numberOfResults: state.eventsMetaData.numberOfResults
    }
}
EventsDashboardHeader.propTypes = {
        dispatch: func,
        numberOfResults: number
    }

export default connect(mapStateToProps)(EventsDashboardHeader)
