import React from 'react';

class SongkickAttribution extends React.Component {

    render() {
        return(
            <section className="songkick-attribution-container">
                <img src="/images/powered-by-songkick-white.png"/>
            </section>
        )
    }
}

module.exports = SongkickAttribution;
