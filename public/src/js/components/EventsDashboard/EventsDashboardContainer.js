import React from 'react';
import EventsDashboardHeader from './EventsDashboardHeader'
import EventCardsContainer from './Events/EventCardsContainer';
import Menu from './Menu';
import SongkickAttribution from './SongkickAttribution';

class EventsDashboardContainer extends React.Component {
    constructor(props) {
      super(props);
    }

    render() {
        return(
            <section className="search-container">
                <Menu/><EventsDashboardHeader/>
                <EventCardsContainer/>
                <SongkickAttribution/>
            </section>
        )
    }
}

module.exports = EventsDashboardContainer;
