import React from 'react';
import XIcon from '../SVG/XIcon';
import { connect } from 'react-redux'
import { toggleMenuOverlay } from '../../store/actionCreators'

class Menu extends React.Component {
    constructor(props) {
        super(props);
        this.toggleMenuOverlay = this.toggleMenuOverlay.bind(this);
    }

    toggleMenuOverlay(){
        this.props.dispatch(toggleMenuOverlay())
    }

    render() {
        return(
            <section className={'menu-overlay ' + (this.props.showMenu ? 'is-open':'is-closed')}>
                <header onClick={this.toggleMenuOverlay}>
                    <XIcon/>
                </header>

                <main>
                    <p>Thanks for checking us out. </p>
                    <p>&nbsp;</p>
                    <p>We are still very much in  development mode, so please excuse bugs and features that don't work yet.</p>
                    <p>&nbsp;</p>
                    <p>And stay tuned!</p>
                </main>

                <footer className="menu-overlay-footer">
                    <div className="contact-us">Questions? Feedback? Ideas? <br/> Get in touch!</div>
                    <a className="contact email" href="mailto:info@showmethemusic.co">info@showmethemusic.co</a>
                    <a className="contact twitter" href="http://twitter.com/intent/tweet?screen_name=ShowMeTheMusic4">@ShowMeTheMusic4</a>
                </footer>

            </section>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        showMenu: state.showMenu
    }
}

const { bool, func } = React.PropTypes;

Menu.propTypes = {
        showMenu: bool,
        dispatch: func,
    }

export default connect(mapStateToProps)(Menu);
