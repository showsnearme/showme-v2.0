import React from 'react';
import XIcon from '../SVG/XIcon';
import PlayIcon from '../SVG/PlayIcon';

import TopRow from './TopRow';
import BottomRow from './BottomRow';

import { connect} from 'react-redux';
import { closeEventDetailsOverlay, toggleEventOnPlaylist } from '../../store/actionCreators'


class EventDetailsOverlayContainer extends React.Component {
    constructor(props){
        super(props)
        const this_component = this;
        this.closeEventDetailsOverlay = this.closeEventDetailsOverlay.bind(this)
        this.toggleEventSelectedState = this.toggleEventSelectedState.bind(this)
    }

    closeEventDetailsOverlay(){
        this.props.dispatch(closeEventDetailsOverlay())
    }

    toggleEventSelectedState(){
        this.props.dispatch(toggleEventOnPlaylist(this.props.selectedEvent))
        this.closeEventDetailsOverlay();
    }

    render() {
        let labelText = null;

        if (this.props.selectedEvent){
            if (this.props.selectedEvent.isSelected)
                labelText = 'remove event from playlist'
            else
                labelText = 'add event to playlist'
        }

        return(
            <section className={'artist-overlay ' + (this.props.showEventDetailsOverlay ? '' : 'hidden')} >
                <div className="artist-overlay-wrapper">
                    <div className="table-container">
                        <div className="table-cell-container">
                            <div className="artist-overlay-content-box">
                                <div className="x-button" onClick={this.closeEventDetailsOverlay}>
                                    <XIcon />
                                </div>

                                <TopRow genres={this.props.genres} venue={this.props.venue} selectedEvent={this.props.selectedEvent}/>
                                <BottomRow similarArtists={this.props.similarArtists} venue={this.props.venue}/>

                                <section className="add-to-playlist-footer" onClick={this.toggleEventSelectedState}>
                                    <PlayIcon />
                                    <span className={"play-button-label " + (this.props.selectedEvent.isSelected ? 'selected': '')}>{labelText}</span>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

/**********************
  PROP TYPE VALIDATIORS
  **********************/
const {object, bool, array, arrayOf, string, func} = React.PropTypes;

const mapStateToProps = (state) => {
    return {
        selectedEvent: state.eventDetailsOverlay.selectedEvent,
        showEventDetailsOverlay: state.eventDetailsOverlay.showEventDetailsOverlay,
        similarArtists: state.eventDetailsOverlay.similarArtists,
        genres: state.eventDetailsOverlay.genres,
        venue: state.eventDetailsOverlay.venue,
    }
}

EventDetailsOverlayContainer.propTypes = {
    selectedEvent: object,
    showEventDetailsOverlay: bool,
    genres: array,
    similarArtists: array,
    vanue: object,
    dispatch: func
};

export default connect(mapStateToProps)(EventDetailsOverlayContainer);
