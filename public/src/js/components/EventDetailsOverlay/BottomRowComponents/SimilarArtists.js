import React from 'react';

class EventDetailsOverlayBottomRow extends React.Component {
    render() {
        var avatar1, avatar2, avatar3 = null;
        var similarArtist1,similarArtist2, similarArtist3 = null;
        var similarArtistsUrl1, similarArtistsUrl2, similarArtistsUrl3 = null;

        try{
            similarArtistsUrl1 = this.props.similarArtists[0].url ? this.props.similarArtists[0].url : null;
            similarArtistsUrl2 = this.props.similarArtists[1].url ? this.props.similarArtists[2].url : null;
            similarArtistsUrl3 = this.props.similarArtists[2].url ? this.props.similarArtists[2].url : null;
        }catch(e){
            similarArtistsUrl1 = null;
            similarArtistsUrl2 = null;
            similarArtistsUrl3 = null;
        }

        try{
            var similarArtist1 = this.props.similarArtists[0].name ? this.props.similarArtists[0].name : 'Sorry could not find any related artists.';
            var similarArtist2 = this.props.similarArtists[1].name ? this.props.similarArtists[1].name : null;
            var similarArtist3 = this.props.similarArtists[2].name ? this.props.similarArtists[2].name : null;
        }catch(e){
            var similarArtist1 = 'Sorry could not find any related artists.';
            var similarArtist2 = null;
            var similarArtist3 = null;
        }

        try{
            var avatar1 = this.props.similarArtists[0].image[0]['#text'] ? {"backgroundImage": "url(" + this.props.similarArtists[0].image[1]['#text'] + ")"} : {"backgroundColor": "rgba(109, 126, 117, 0.85)"};
            var avatar2 = this.props.similarArtists[1].image[1]['#text'] ? {"backgroundImage": "url(" + this.props.similarArtists[1].image[1]['#text'] + ")"} : {"backgroundColor": "rgba(109, 126, 117, 0.85)"};
            var avatar3 = this.props.similarArtists[2].image[2]['#text'] ? {"backgroundImage": "url(" + this.props.similarArtists[2].image[1]['#text'] + ")"} : {"backgroundColor": "rgba(109, 126, 117, 0.85)"};
        }catch(e){
            var avatar1 = {"backgroundColor": "rgba(109, 126, 117, 0.85)"}
            var avatar2 = {"backgroundColor": "rgba(109, 126, 117, 0.85)"}
            var avatar3 = {"backgroundColor": "rgba(109, 126, 117, 0.85)"}
        }

        return(
            <div className="left-side-container">
                <div className="related-artists-title">
                    Related Artists
                </div>
                <ul className="related-artists-list">
                    <li className="related-artist one">
                        <div style={avatar1} className="related-artist-image"></div>
                        <span className="related-artist-name"><a href={similarArtistsUrl1} target="_blank">{similarArtist1}</a></span>
                    </li>
                    <li className="related-artist two">
                        <div style={avatar2} className="related-artist-image"></div>
                        <span className="related-artist-name"><a href={similarArtistsUrl2} target="_blank">{similarArtist2}</a></span>
                    </li>
                    <li className="related-artist three">
                        <div style={avatar3} className="related-artist-image"></div>
                        <span className="related-artist-name"><a href={similarArtistsUrl3} target="_blank">{similarArtist3}</a></span>
                    </li>
                </ul>
            </div>
        )
    }
}

module.exports = EventDetailsOverlayBottomRow;
