import React from 'react';
import GoogleMap from 'google-map-react'
import GoogleMapMarker from '../../SVG/GoogleMapMarker'
import styles from './mapstyles'
import { connect} from 'react-redux';

const QuestionMark = () => (
    <div className="question-mark-marker">?</div>
)

class VenueMap extends React.Component {
  render() {
    let zoom = 15, isKnownLocation = true, KEY = {key: 'AIzaSyCY591DoZl4S6hHC7xyWUc3V8rbuy7xE9w'};
    let center = {
      lat: this.props.venue.lat,
      lng: this.props.venue.lng
    }
 
    if (!center.lat && !center.lng){
        center = {
            lat: this.props.location.lat,
            lng: this.props.location.lng
        }
        zoom = 11;
        isKnownLocation = false;
    }

    return (
        <div className="right-side-container">
            <GoogleMap
                bootstrapURLKeys={KEY}
                resetBoundsOnResize={true}
                center={center}
                zoom={zoom}
                options={styles}
                >
                {isKnownLocation ? <GoogleMapMarker lat={center.lat} lng={center.lng}/> : <QuestionMark lat={center.lat} lng={center.lng}/>}
            </GoogleMap>
          </div>
    );
  }
}

/**********************
  PROP TYPE VALIDATIORS
  **********************/
const {object, shape, number} = React.PropTypes;

VenueMap.PropTypes = {
    venue: shape({
        lat: number,
        lng: number,
    }),
    location: shape({
        lat: number,
        lng: number,
    })
}

const mapStateToProps = (state) => {
    return {
        location: state.location,
    }
}

export default connect(mapStateToProps)(VenueMap);
