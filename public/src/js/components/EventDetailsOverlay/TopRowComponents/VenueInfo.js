import React from 'react';
import songkick from '../../../services/songkick';

class VenueInfo extends React.Component {
    constructor(props){
        super(props)
    }

    render() {
        var regionalAddress = null;
        var venueUri, venueDisplayName, venueStreet = null;
        try{
            if (this.props.venue.city.state)
                 regionalAddress = this.props.venue.city.displayName + ', ' + this.props.venue.city.state.displayName + ' ' + this.props.venue.zip;
            else
                regionalAddress = this.props.venue.city.displayName + ', ' + this.props.venue.city.country.displayName + ' ' + this.props.venue.zip;
        }catch(e){
            regionalAddress = null;
        }

        try{
            venueUri = this.props.venue.uri ? this.props.venue.uri : null;
            venueDisplayName = this.props.venue.displayName ? this.props.venue.displayName : 'venue not available';
            venueStreet = this.props.venue.street ? this.props.venue.street : null;
        }catch(e){
            venueUri = null;
            venueDisplayName = null;
            venueStreet = null;
        }

        return(
            <span className="venue">
               <div className="venue-name"><a target="_blank" href={venueUri}>{venueDisplayName}</a></div>
               <div className="venue-address supporting-info">{venueStreet}</div>
               <div className="venue-city-and-zip supporting-info">{regionalAddress}</div>
            </span>
        )
    }
}

module.exports = VenueInfo;
