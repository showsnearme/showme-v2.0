import React from 'react';

class GenreInfo extends React.Component {
    constructor(props){
        super(props)
    }

    render() {
        var genres, genreUrl, genreName = null;

        try{
            if (this.props.genres.length > 0){
                var genres = this.props.genres.map( (genre, ind) => {
                    var genreUrl = genre.url ? genre.url : null;
                    var genreName = genre.name ? genre.name.toLowerCase() : null;

                    if (ind === this.props.genres.length - 1){
                        return(
                            <span key={ind}><a target="_blank" href={genreUrl}> {genreName} </a> </span>
                        )
                    }else{
                        return(
                            <span key={ind}><a target="_blank" href={genreUrl}> {genreName} </a> {String.fromCharCode(183)} </span>
                        )
                    }
                });
            }else{
                genres = 'Cannot find any genre information'
            }

        }catch(e){
            genres = 'Cannot find any genre information';
        }


        return(
            <span className="sort-option-label">
                 {genres}
            </span>

        )
    }
}

module.exports = GenreInfo;
//&#8226
