import React from 'react';

class ArtistInfo extends React.Component {
    constructor(props){
        super(props)
    }
    render() {
        var headliners = null;
        var supportingActs = null;

        if (this.props.performers.length > 0){

            headliners = this.props.performers.filter( (performer) => {
                return performer.isHeadliner
            }).map( (headliner, index) => {
                if (index > 0){
                    return (
                        <span key={index} className="headliner-label">, <a href={headliner.songkick.uri} target="_blank">{headliner.displayName}</a></span>
                    )
                }else{
                    return (
                        <span key={index} className="headliner-label"><a href={headliner.songkick.uri} target="_blank">{headliner.displayName}</a></span>
                    )
                }
            });

            var supportingActs = this.props.performers.filter( (performer) => {
                return !performer.isHeadliner
            }).map( (supportingAct, index) => {
                if (index > 0 ){
                    return(
                        <div key={index} className="supporting-artist-label">
                            <div className="bullet-separator">&amp;</div>
                            <div className="supporting-artist"> <a href={supportingAct.songkick.uri} target="_blank">{supportingAct.displayName}</a></div>
                        </div>
                    )
                }else {
                    return (
                        <div key={index} className="supporting-artist-label">
                            <div className="featuring">Featuring:&nbsp;</div>
                            <div className="supporting-artist"> <a href={supportingAct.songkick.uri} target="_blank">{supportingAct.displayName} </a> </div>
                        </div>
                    )
                }
            });

        }

        return(
            <span className="performing-artists">
                {headliners}
               <div className="supporting-artists">
                   {supportingActs}
                </div>
           </span>
        )
    }
}

module.exports = ArtistInfo;
