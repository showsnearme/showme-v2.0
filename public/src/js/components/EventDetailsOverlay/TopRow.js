import React from 'react';

import VenueIcon from '../SVG/VenueIcon';
import MicrophoneIcon from '../SVG/MicrophoneIcon';
import ArtistIcon from '../SVG/ArtistIcon';
import CalendarIcon from '../SVG/CalendarIcon';

import ArtistInfo from './TopRowComponents/ArtistInfo';
import VenueInfo from './TopRowComponents/VenueInfo';
import GenreInfo from './TopRowComponents/GenreInfo';

class EventDetailsOverlayTopRow extends React.Component {
    constructor(props){
        super(props)
    }

    render() {
        var url, inlineStyles;

        if (this.props.selectedEvent.performers && this.props.selectedEvent.performers[0]){
            if (this.props.selectedEvent.performers[0].images[0] && this.props.selectedEvent.performers[0].images[0].url){
                url =  this.props.selectedEvent.performers[0].images[0].url;
                inlineStyles= {
                    "backgroundImage": "url(" + url + ")"
                }
            };
        }


        return(
            <section className="top-row">
                <div className="headliner-image-container" style={inlineStyles}>
                    <div className="headliner-image"></div>
                </div>
                <ul className="event-details">
                    <li className="event-detail artist">
                        <ArtistIcon/>
                        <ArtistInfo performers={this.props.selectedEvent.performers}/>
                    </li>
                    <li className="event-detail starttime">
                        <CalendarIcon/>
                        <span className="showtime">
                            {this.props.selectedEvent.start.displayDay}, {this.props.selectedEvent.start.displayDate} {this.props.selectedEvent.start.displayTime ? '@ ' + this.props.selectedEvent.start.displayTime : ''}</span>
                    </li>
                    <li className="event-detail venue-info">
                        <VenueIcon/>
                        <VenueInfo venue={this.props.venue}/>
                    </li>
                    <li className="event-detail genres">
                        <MicrophoneIcon/>
                        <GenreInfo genres={this.props.genres} />
                    </li>
                </ul>
            </section>
        )
    }
}

module.exports = EventDetailsOverlayTopRow;
