import React from 'react';
import lastfm from '../../services/lastfm';
import SimilarArtists from './BottomRowComponents/SimilarArtists';
import VenueMap from './BottomRowComponents/VenueMap';

class EventDetailsOverlayBottomRow extends React.Component {
    render(){
        return(
            <section className="bottom-row">
                <SimilarArtists similarArtists={this.props.similarArtists}/>
                <VenueMap venue={this.props.venue}/>
            </section>
        )
    }
}

module.exports = EventDetailsOverlayBottomRow;
