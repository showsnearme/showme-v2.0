import _ from 'underscore';

export const getSpotifyArtistIds = function(evts){
    var selectedArtistIds = _.filter(evts, (evt) => {
        return evt.isSelected;
    }).map((evt) => {
        return _.pluck(evt.performers, 'spotify');
    });

    selectedArtistIds = _.flatten(selectedArtistIds);
    selectedArtistIds = _.pluck(selectedArtistIds, 'artist_id');
    selectedArtistIds = _.filter(selectedArtistIds, (id)=>{
        return id
    })

    return selectedArtistIds;
}

export const getPlaylistDescription = function(evts){
    var spotify_description_without_line_breaks = '',
        description = '',
        genres = [],
        starttime = '',
        venue = '',
        selectedArtists = [];

    var selectedEvents = _.filter(evts, (evt) => {
        return evt.isSelected;
    }).map((evt) => {
        selectedArtists = _.pluck(evt.performers, 'spotify');
        genres = evt.performers && evt.performers[0].genres ? evt.performers[0].genres.slice(0,2) : '';
        starttime = evt.start && evt.start.displayTime ? evt.start.displayTime : '';
        venue = evt.venue && evt.venue.displayName ? evt.venue.displayName : '';

        return {
            selectedArtists: _.pluck(selectedArtists, 'displayName'),
            genres: genres,
            starttime: starttime,
            venue: venue
        }
    });

    selectedEvents.forEach((evt)=>{
        spotify_description_without_line_breaks = spotify_description_without_line_breaks + evt.selectedArtists.toString() +  ' - ' + evt.venue + ', @' + evt.starttime + '| ';
        description = description + '<strong>' + evt.selectedArtists.toString() + '</strong>' +  ' - ' + evt.venue + ', @' + evt.starttime + ', ' + evt.genres.toString() + '<br />';
    });

    return {
        spotify_description_without_line_breaks: spotify_description_without_line_breaks,
        description: description
    }
}

export const getDefaultPlaylistName = function(minDate, maxDate, location){
    const min_date = minDate.format('dddMMMDD'); //FriOct13
    const max_date = maxDate.format('dddMMMDD'); //SunOct15
    let locationName = '';

    //Only take 1st part of the location string
    if (location && location.displayName){
        var ind = location.displayName.indexOf(',');
        if (ind > 0){
            locationName = location.displayName.substring(0,ind).split('');
        }else{
            locationName = location.displayName.split('');
        }
        locationName[0] = locationName[0].toUpperCase();

        //Make CamelCase
        for (var i=1; i<locationName.length; i++){
            if (locationName[i] === ' '){
                locationName[i+1] = locationName[i+1].toUpperCase()
                locationName.splice(i,1);
            }
        }

        return locationName.join('');
    }
}

export const getPlaceholderName = function(minDate, maxDate, locationName){
    let placeholder = '';

    if (minDate.isSame(maxDate, 'day')){
        placeholder = minDate.format('dddMMMDD') + '-' + locationName;
    }else{
        placeholder = minDate.format('dddMMMDD') + '-' + maxDate.format('dddMMMDD') + '-' + locationName
    }

    return placeholder
}
