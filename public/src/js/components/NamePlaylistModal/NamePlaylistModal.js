import React from 'react';
import XIcon from '../SVG/XIcon';
import PlayIcon from '../SVG/PlayIcon';
import { form } from '../../services/jQueryExtension';
import { momentPropType } from '../../utils/propTypes';
import { getSpotifyArtistIds, getPlaylistDescription, getDefaultPlaylistName, getPlaceholderName } from './helpers';
import { connect } from 'react-redux';
import { toggleNamePlaylistModal } from '../../store/actionCreators'


class NamePlaylistModal extends React.Component {
    constructor(props){
        super(props)

        this.closeModal = this.closeModal.bind(this)
        this.generatePlaylist = this.generatePlaylist.bind(this)
    }

    generatePlaylist(){
        const playlistNameInput = this.playlistName;
        const playlistName = playlistNameInput.value ? playlistNameInput.value : playlistNameInput.getAttribute('placeholder');
        const playlistDescription = getPlaylistDescription(this.props.events);

        const data = {
            playlistName: playlistName,
            selectedArtistIds: getSpotifyArtistIds(this.props.events),
            description: playlistDescription.description,
            spotify_description_without_line_breaks: playlistDescription.spotify_description_without_line_breaks
        }

        //Because spotify won't let you authorize with AJAX -- https://github.com/JMPerez/passport-spotify/issues/6
        //(You get a 'Access-Control-Allow-Origin' header is present on the requested resource."' response from spotify)
        //We need to simulate making an ole fashioned form and POST to our server
        //Normally, we could just make a GET by changing the href, but in our case we also need to POST data along as well.


        form('/api/spotify/playlist', { data: data}).submit();


    }

    closeModal(){
        this.props.dispatch(toggleNamePlaylistModal());
    }

    render() {
        const locationName = getDefaultPlaylistName(this.props.minDate, this.props.maxDate, this.props.location);
        const placeholder = getPlaceholderName(this.props.minDate, this.props.maxDate, locationName);

        return(
            <section className= {`modal-overlay-container ${this.props.showNamePlaylist ? '': 'hidden'}`}>
                <div className="modal-overlay-wrapper">
                    <div className="table-container">
                        <div className="table-cell-container">
                            <div className="modal-overlay-content-box">
                                <div className="prompt">Would you like to name your playlist?</div>
                                <input className="playlist-name" ref={node => this.playlistName = node} type="text" placeholder={placeholder}/>
                                <div onClick={this.closeModal} className="go-back">
                                    <XIcon/>
                                </div>

                                <footer className="listen-to-artists" onClick={this.generatePlaylist}>
                                    <div className="listen-to-selected-artists-container">
                                        <PlayIcon />
                                        <div className="play-button label">generate my spotify playlist</div>
                                    </div>
                                </footer>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

/**********************
  PROP TYPE VALIDATIORS
  **********************/

const { arrayOf, shape, object, string, number, bool } = React.PropTypes;

NamePlaylistModal.propTypes = {
    minDate: momentPropType,
    maxDate: momentPropType,
    events: arrayOf(object),
    location: shape({
        displayName: string,
        songkick_locationid: number
    }),
    showNamePlaylist: bool
};


const mapStateToProps = (state) =>{
    return {
        events: state.events,
        maxDate: state.maxDate,
        minDate: state.minDate,
        location: state.location,
        showNamePlaylist: state.showNamePlaylist
    }
}

export default connect(mapStateToProps)(NamePlaylistModal);
