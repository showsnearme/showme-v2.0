import React from 'react';
import ReactDOM from 'react-dom';
import Marquee from './Marquee/Marquee';
import Spinner from './SVG/Spinner';
import EventsDashboard from './EventsDashboard/EventsDashboardContainer';
import ListenToArtistsFooter from './ListenToArtistsFooter/ListenToArtistsFooter';
import EventDetailsOverlay from './EventDetailsOverlay/EventDetailsOverlayContainer';
import NamePlaylistModal from './NamePlaylistModal/NamePlaylistModal';

import { momentPropType } from '../utils/propTypes';

import apiUtil from '../services/apiUtil'
import songkick from '../services/songkick';
import lastfm from '../services/lastfm';
import moment from 'moment';
import async from 'async';
import Rx from 'rx';
import $ from 'jquery';

import { connect} from 'react-redux';
import { showSearchResultsSpinner, getEventsFromLatitudeAndLongitude, updateEvents, incremementPage } from '../store/actionCreators'


class App extends React.Component {
    constructor(){
        super();
        this.resetNamePlaylistModalVisibility = this.resetNamePlaylistModalVisibility.bind(this);
        this.toggleInputs = this.toggleInputs.bind(this);
        this.noLocationFound = this.noLocationFound.bind(this);
    }

    componentWillMount(){
        //Temporary forwarding mechanism because this link was put on resumes
        if (window.location.hostname.indexOf('showsnearme-sara.herokuapp.com') >= 0 || window.location.hostname.indexOf('http://showsnearme.co') >=0){
            window.location.assign('http://showmethemusic.co');
        }

        try{
            apiUtil.getIPaddress((err, ip) => {
                if (!err){
                    apiUtil.getLocationFromIpAddress(ip, (err, locationDisplayName, latitudeFromIp, longitudeFromIp) => {
                        if (!err){
                            this.props.dispatch(showSearchResultsSpinner())
                            this.props.dispatch(getEventsFromLatitudeAndLongitude(latitudeFromIp, longitudeFromIp))
                        }else{
                            this.noLocationFound()
                        }
                    })
                }else{
                    this.noLocationFound()
                }
            })
        }catch(e){
            this.noLocationFound()
        }

    }
    componentDidMount(){
        const hasReachedBottomOfPage = Rx.Observable.fromEvent(window, 'scroll')
            .map((e) => {
                var scrollPosition = window.scrollY;
                var windowHeight = window.innerHeight;
                var documentHeight = document.body.scrollHeight;

                return scrollPosition >= (Math.ceil(documentHeight - windowHeight));
            })
            .filter((atBottomOfPage) => {
                return atBottomOfPage
            })

        hasReachedBottomOfPage.subscribe(
            () => {
                const currentPage = this.props.eventsMetaData.currentPage;
                const numberOfPages = this.props.eventsMetaData.numberOfPages;
                let page;

                if (currentPage < numberOfPages){
                    page = currentPage + 1;
                    this.props.dispatch(showSearchResultsSpinner());
                    this.props.dispatch(updateEvents({page}));

                }else{
                    console.log('No more data');
                }
            },
            (err) => {
                console.log(err)
            }
        )
    }

    noLocationFound(){
        this.props.dispatch(updateEvents())  //update Events with current state
    }

    resetNamePlaylistModalVisibility(state){
        this.setState(
            {namePlaylistModalVisibilitityState: state}
        );
    }

    toggleInputs(evt){
        //if its a request to open something, open it and close everything else.
        if ( $(evt.target).hasClass('min-date')){
            $('.date-selector.min-date .dropdown .dropdown-content').removeClass('hidden');
            $('.date-selector.min-date .dropdown .dropdown-content').addClass('show-block');
            $('.date-selector.max-date .dropdown .dropdown-content').addClass('hidden');
            $('.location.dropdown-content').addClass('hidden');
        }else if ( $(evt.target).hasClass('max-date')){
            $('.date-selector.max-date .dropdown .dropdown-content').removeClass('hidden');
            $('.date-selector.max-date .dropdown .dropdown-content').addClass('show-block');

            $('.date-selector.min-date .dropdown .dropdown-content').addClass('hidden');
            $('.location.dropdown-content').addClass('hidden');
        }else if ( $(evt.target).hasClass('.location-input')){
            $('.date-selector.max-date .dropdown .dropdown-content').addClass('hidden');
            $('.date-selector.min-date .dropdown .dropdown-content').addClass('hidden');
        }else{
            if ($(evt.target).hasClass('prev-month') || $(evt.target).hasClass('next-month')){
                //do nothing
            }else{
                $('.date-selector.max-date .dropdown .dropdown-content').addClass('hidden');
                $('.date-selector.min-date .dropdown .dropdown-content').addClass('hidden');
                $('.location.dropdown-content').addClass('hidden');
            }
        }
    }

    render() {
        return(
            <div>
                <div onClick={this.toggleInputs}>
                    <Marquee/>
                    <EventsDashboard/>
                    <EventDetailsOverlay/>
                    <NamePlaylistModal/>
                    <ListenToArtistsFooter/>
                </div>

             </div>
       )
    }
}

const mapStateToProps = (state) => {
    return {
        eventsLoading: state.eventsLoading,
        eventsMetaData: state.eventsMetaData
    }
}

const { object, bool, func } = React.PropTypes;

App.propTypes = {
    eventsMetaData: object,
    eventsLoading: bool,
    dispatch: func,
};

export default connect(mapStateToProps)(App);
