import React from 'react';
import moment from 'moment';
import Week from './Week';
import { momentPropType } from '../../utils/propTypes';

class Calendar extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            currentCalendarDisplay: this.props.selectedDate.clone()
        };

        this.showPreviousMonth = this.showPreviousMonth.bind(this);
        this.showNextMonth = this.showNextMonth.bind(this);
    }

    componentWillReceiveProps(nextProps){
        if (nextProps.selectedDate){
            this.setState({
                currentCalendarDisplay: nextProps.selectedDate.clone()
            })
        }
    }

    showNextMonth(){
        const this_component = this;
        this.setState({
            currentCalendarDisplay: this_component.state.currentCalendarDisplay.add(1, 'months')
        });
    }

    showPreviousMonth(){
        const this_component = this;
        this.setState({
            currentCalendarDisplay: this_component.state.currentCalendarDisplay.subtract(1, 'months')
        });
    }

    render() {
        const currentCalendarDisplay = this.state.currentCalendarDisplay.clone();
        const displayMonth = currentCalendarDisplay.format('MMMM');
        const displayYear = currentCalendarDisplay.format('YYYY');
        const startingDatesOfTheWeek = generateCalendarInformationFrom(currentCalendarDisplay);

        const Weeks = startingDatesOfTheWeek.map((startingDateOfWeek, index) =>{
            return (
                <Week key={index}
                    startingDateOfWeek={startingDateOfWeek}
                    currentCalendarDisplay={currentCalendarDisplay}
                    dateType={this.props.dateType}/>
            )
        });

        return(
            <div className="calendar">
                <div className="header month-and-year">
                    <span className="prev-month btn" onClick={this.showPreviousMonth}>&lt;</span>
                    <span className="title">
                        <span className="month">{displayMonth}&nbsp;</span>
                        <span className="year">{displayYear}</span>
                    </span>
                    <span className="next-month btn" onClick={this.showNextMonth}>&gt;</span>
                </div>

                <div className="main-container">
                    <div className="days-of-the-week">
                        <span className="day-of-the-week">Su</span>
                        <span className="day-of-the-week">Mo</span>
                        <span className="day-of-the-week">Tu</span>
                        <span className="day-of-the-week">We</span>
                        <span className="day-of-the-week">Th</span>
                        <span className="day-of-the-week">Fr</span>
                        <span className="day-of-the-week">Sa</span>
                    </div>

                    <div className="dates-of-the-month">
                        {Weeks}
                    </div>

                </div>
            </div>
        )
    }
}

/*
 This function will generate an array.
 Each value in the array represents 1st valid date of in each week of the month (usually a Sunday).
 For example November 2016 would have an array of [1,6,13,20,27] representing Tue Nov1, Sun Nov6, Sun Nov13, Sun Nov20, Sun Nov27
 */
function generateCalendarInformationFrom(MomentObj){
    var firstWeekdayOfCurrentMonth = MomentObj.date(1).day();
    var numOfWeeksInMonth = getNumberOfWeeksForMonth(MomentObj.year(), MomentObj.month());
    var startingDatesOfTheWeek = [];
    startingDatesOfTheWeek[0] = 1;
    startingDatesOfTheWeek[1] = startingDatesOfTheWeek[0] + (7 - firstWeekdayOfCurrentMonth);


    for (var i = 2; i< numOfWeeksInMonth; i++){
        startingDatesOfTheWeek[i] = startingDatesOfTheWeek[i-1]+7;
    }

    return startingDatesOfTheWeek;
}

function getNumberOfWeeksForMonth(year,month){
      var date = new Date(year,month,1);
      var day = date.getDay();
      var numDaysInMonth = new Date(year, month, 0).getDate();
      return Math.ceil((numDaysInMonth + day) / 7);
}
//reference: http://stackoverflow.com/questions/28384869/moment-js-get-number-of-weeks-in-a-month


Calendar.propTypes = {
    dateType: React.PropTypes.string,
    selectedDate: momentPropType
};


export default Calendar;
