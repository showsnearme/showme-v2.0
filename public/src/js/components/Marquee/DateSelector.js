import Calendar from './Calendar';
import React from 'react';
import moment from 'moment';
import { momentPropType } from '../../utils/propTypes';


class DateSelector extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
      return (
        <div className="dropdown date">
          <span className={"dropbtn " + this.props.dateType}>
              {this.props.selectedDate.format('ddd MMM Do')}
          </span>

          <div className='date dropdown-content hidden'>
              <Calendar
                  dateType={this.props.dateType}
                  selectedDate={this.props.selectedDate}
              />
          </div>
        </div>
    );
  }
}

/**********************
  PROP TYPE VALIDATIORS
  **********************/

DateSelector.propTypes = {
    dateType: React.PropTypes.string,
    selectedDate: momentPropType,
};

export default DateSelector;
