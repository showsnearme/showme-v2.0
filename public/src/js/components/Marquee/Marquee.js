import React from 'react';
import DateSelector from './DateSelector';
import LocationSelector from './LocationSelector';
import {connect} from 'react-redux';
import { momentPropType } from '../../utils/propTypes';

class Marquee extends React.Component {
    constructor(props) {
      super(props);
    }

    render() {
        return(
            <header className="marquee">
                <div className="greeting">
                    <span className="mobile line one">show me music&nbsp;</span>
                    <span className="mobile line two">I can see</span><br/>
                    <span className="date-selector min-date">
                        <DateSelector dateType="min-date" selectedDate={this.props.minDate}/>
                        <span className="conjunction-text to">&nbsp;to&nbsp;</span>
                    </span>

                    <span className="date-selector max-date">
                        <DateSelector dateType="max-date" selectedDate={this.props.maxDate}/>
                        <span className="conjunction-text in">&nbsp;in&nbsp;</span>
                    </span>
                    
                    <span className="location-selector"><LocationSelector/></span>
                </div>
            </header>
        )
    }
}

Marquee.propTypes = {
    minDate: momentPropType,
    maxDate: momentPropType,
    dispatch: React.PropTypes.func
};

const mapStateToProps = (state) => {
    return {
        minDate: state.minDate,
        maxDate: state.maxDate
    }
};

export default connect(mapStateToProps)(Marquee)
