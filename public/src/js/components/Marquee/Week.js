import React from 'react';
import moment from 'moment';
import {connect} from 'react-redux';
import { updateEvents, showSearchResultsSpinner, clearEvents } from '../../store/actionCreators';
import { momentPropType } from '../../utils/propTypes';

class Weeks extends React.Component {
    constructor(props){
        super(props)
        this.updateDate = this.updateDate.bind(this);
    }

    updateDate(e) {
        var day = e.target.innerHTML;

        const selectedDate = moment(this.props.currentCalendarDisplay.year() + '-' + (this.props.currentCalendarDisplay.month()+1) + '-' + day, "YYYY-MM-DD");

        this.props.dispatch(clearEvents())
        this.props.dispatch(showSearchResultsSpinner())

        if (this.props.dateType === "min-date")
           this.props.dispatch(updateEvents({min_date: selectedDate}))

        if (this.props.dateType === "max-date")
           this.props.dispatch(updateEvents({max_date: selectedDate}))
     }

    render() {

        var currentCalendarDisplay = this.props.currentCalendarDisplay.clone();

        var firstWeekdayOfCurrentMonth = currentCalendarDisplay.date(1).day();
        var numDaysInCurrentMonth = currentCalendarDisplay.daysInMonth();
        var lastWeekdayOfCurrentMonth = currentCalendarDisplay.date(numDaysInCurrentMonth).day();
        var datesOfTheWeek = [0,1,2,3,4,5,6];

        if (this.props.startingDateOfWeek === 1){
            for (var i = 0; i<firstWeekdayOfCurrentMonth; i++){
                datesOfTheWeek[i] = 0;
            }
            var count = 0;

            for (var i = firstWeekdayOfCurrentMonth; i<datesOfTheWeek.length; i++){
                datesOfTheWeek[i] = this.props.startingDateOfWeek + count;
                count++;
            }

        }else if (this.props.startingDateOfWeek + 7 > numDaysInCurrentMonth){
            var count = 0;
            for (var i = 0; i<=lastWeekdayOfCurrentMonth; i++){
                datesOfTheWeek[i] = this.props.startingDateOfWeek + count;
                count++;
            }

            for (var i = count; i<datesOfTheWeek.length; i++){
                datesOfTheWeek[i] = 0;
            }
            //var datesOfTheWeek = [29,30,31,0,0,0,0];
        }else{
            datesOfTheWeek = datesOfTheWeek.map((date) => {
                return date + parseInt(this.props.startingDateOfWeek)
            })
            //var datesOfTheWeek = [8,9,10,11,12,13,14];
        }

        var Week = datesOfTheWeek.map((date, index) => {
            var currentDisplayDate = this.props.currentCalendarDisplay.clone();
            currentDisplayDate.date(date);
            var today = moment();


            if (date === 0 ){
                return(
                    <span key={index} className="date disabled">&nbsp;</span>
                )
            }else if(today.isSame(currentDisplayDate, 'day')){

                return(
                    <span key={index} onClick={this.updateDate} className="date today">{date}</span>
                )
            }else if(today.isAfter(currentDisplayDate, 'day')){
                return(
                    <span key={index} className="date disabled">{date}</span>
                )
            }else{
                return(
                    <span key={index} onClick={this.updateDate} className="date">{date}</span>
                )
            }
        });

        return(
            <div className="week">
                {Week}
            </div>
        )
    }
}

Weeks.propTypes = {
    key: React.PropTypes.number,
    startingDateOfWeek: React.PropTypes.number,
    currentCalendarDisplay: React.PropTypes.object,
    dateType: React.PropTypes.string,
    minDate: momentPropType,
    maxDate: momentPropType,
    dispatch: React.PropTypes.func
};

const mapStateToProps = (state) => {
    return {
        minDate: state.minDate,
        maxDate: state.maxDate
    }
};

export default connect(mapStateToProps)(Weeks);
