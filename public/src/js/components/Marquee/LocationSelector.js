import React from 'react';
import Rx from 'rx';
import { updateEvents, showSearchResultsSpinner, clearEvents } from '../../store/actionCreators';
import {connect} from 'react-redux';

class LocationSelector extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
          locationOptions: []
        };

      this.updateLocation = this.updateLocation.bind(this);
      this.updatePlaceholderOnClick = this.updatePlaceholderOnClick.bind(this)
      this.returnLastPlaceholderOnBlur = this.returnLastPlaceholderOnBlur.bind(this)

    }
    componentDidMount(){
        var this_component = this;

        var keyup = Rx.Observable.fromEvent(this_component.input, 'keyup')
            .map((e) => {
                return e.target.value
            })
            .distinctUntilChanged();

        // One subscription to set the immediate feedback message in the drop down menu
        keyup.subscribe(
            function (input) {
                this_component.dropdownContent.className = 'location dropdown-content disabled';

                 this_component.setState({
                         locationOptions: [{city: 'searching ', state: 'locations..'}]
                     })
            },
            function (err) {
                console.log('Error: %s', err);
            },
            function () {
                console.log('Completed');
            });


        // Another subscription to turn each keyup event into an ajax call to request data
        keyup.map( (input) => {
            if (input){
                const url = 'api/songkick/location?location=' + input;
                return fetch(url).then(res => res.json())
            }else{
                this_component.dropdownContent.className = 'location dropdown-content hidden';
                return Rx.Observable.never()
            }
        })
        .switchLatest()
        .subscribe(
            function (locations) {

                let noLocationsFound = !locations[0].hasOwnProperty('city') || locations[0].city === 'no locations ',
                    locationOptions = locations;

                if (noLocationsFound){
                    this_component.dropdownContent.className =  'location dropdown-content disabled';
                }else{
                    this_component.dropdownContent.className = 'location dropdown-content';
                }

                this_component.setState({
                    locationOptions
                })
            },
            function (err) {
                console.log('Error:', err);
            },
            function () {
                console.log('Completed');
            });

    }

    updatePlaceholderOnClick(){
        this.input.value = '';
        this.input.placeholder = 'enter a city here';
    }

    returnLastPlaceholderOnBlur(){
        this.input.value = this.props.location.displayName || ' ';
    }

    updateLocation(location){
        
        let locationDisplayName = '';
        if (location.state){
            locationDisplayName = location.city.toLowerCase() + location.state.toLowerCase()
        }else{
            locationDisplayName = location.city.toLowerCase() + ',' + location.country.toLowerCase()
        }


        location.displayName = locationDisplayName;
        this.input.value = location.displayName;

        this.props.dispatch(clearEvents());
        this.props.dispatch(showSearchResultsSpinner())
        this.props.dispatch(updateEvents({
            location: location
        }))
    }

    render() {
        var this_component = this;
        var locationDropDownContent = this.state.locationOptions.map(function(locationOption, index) {
            if (locationOption.state){
                return (
                    <div key={index}
                        onClick={
                            () => this_component.updateLocation(locationOption)
                        }
                        className="location option">
                        {locationOption.city.toLowerCase()}{locationOption.state.toLowerCase()}
                    </div>
                )
            }else{
                return (
                    <div key={index} onClick={() => this_component.updateLocation(locationOption)} className="location option">
                        {locationOption.city.toLowerCase()},{locationOption.country.toLowerCase()}
                    </div>
                )
            }

        });

        return(
        <div className="dropdown location">
            <input className="location-input"
                   ref={(node) => {this.input = node}}
                   placeholder={this.props.location.displayName}
                   type="text"
                   onClick={this.updatePlaceholderOnClick}
                   onBlur={this.returnLastPlaceholderOnBlur}/>
               <div ref={(node => {this.dropdownContent = node})}
                   className="location dropdown-content hidden">
                    {locationDropDownContent}
            </div>
        </div>
    )}
}


const mapStateToProps = (state) => {
    return {
        location: state.location
    }
}

LocationSelector.propTypes = {
    location: React.PropTypes.object,
    dispatch: React.PropTypes.func,
};

export default connect(mapStateToProps)(LocationSelector);

//references:
//http://jsfiddle.net/mattpodwysocki/AL8Mj/
//https://github.com/eliseumds/react-autocomplete
