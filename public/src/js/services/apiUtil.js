import { getJSON } from 'jquery';
import songkick from './songkick'
import lastfm from './lastfm'

exports.songkick = songkick;
exports.lastfm = lastfm;

exports.getIPaddress = function getEvents(callback){
    getJSON('https://api.ipify.org?format=jsonp&callback=?', function (ipdata) {})
      .done((ipdata)=> {
          try{
              var commaIndex = ipdata.ip.indexOf(',');
              var ip;

              if (commaIndex > 0)
                  ip = ipdata.ip.substring(0, commaIndex);
              else
                  ip = ipdata.ip;

              if (ip)
                  callback(null, ip)
              else
                  callback('No ip address could be found', ipdata)
              //"64.223.97.49" -- vermont ip with no location
          }catch(e){
              callback('No ip address could be found', ipdata)
          }
    })
}

exports.getLocationFromIpAddress = function(ip, callback){
    getJSON('https://freegeoip.net/json/' + ip + '?callback=?', function(data){})
    .fail( (err) => {
        callback(err)
    })
    .done((locationFromIP) => {
        console.log('locationFromIP', locationFromIP);
        var locationDisplayName;

        try{
            if (locationFromIP.city && locationFromIP.region_code)
                locationDisplayName = locationFromIP.city + ', ' + locationFromIP.region_code
            else if (locationFromIP.city)
                locationDisplayName = locationFromIP.city
            else
                throw 'Cannot find location information'

            callback(null, locationDisplayName, locationFromIP.latitude, locationFromIP.longitude)
        }catch(e){
            callback(e)
        }
    });
}
