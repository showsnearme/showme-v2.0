exports.getEvents = function getEvents(min_date, max_date, songkick_locationid, page, callback){
    const url = '/api/showme/events?min_date=' + min_date + '&max_date=' + max_date + '&songkick_locationid=' + songkick_locationid + '&page=' + page;
    fetch(url)
        .then(response => {
            if (response.ok)
                return response.json()
            else
                callback(response.json())
        })
        .then( (data) => {
            callback(null, data)
        })
        .catch( (err) => { //network error
            callback(err)
        })
}

exports.getEventsByLatitudeAndLongitude = function getEventsByLatitudeAndLongitude(min_date, max_date, lat, lng, callback){
    const url = '/api/showme/events?min_date=' + min_date + '&max_date=' + max_date + '&lat=' + lat + '&lng=' + lng;
    fetch(url)
        .then(response => {
            if (response.ok)
                return response.json()
            else
                callback(response.json())
        })
        .then( (data) => {
            callback(null, data)
        })
        .catch( (err) => { //network error
            callback(err)
        })
}

exports.getVenueDetails = function getVenueDetails(songkick_venueid, callback){
    const url = '/api/showme/venue?songkick_venueid=' + songkick_venueid;
    fetch(url)
        .then(response => {
            if (response.ok)
                return response.json()
            else
                callback(response.json())
        })
        .then( (data) => {
            callback(null, data)
        })
        .catch( (err) => { //network error
            callback(err)
        })
}
