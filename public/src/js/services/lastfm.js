exports.getSimilarArtistsTo = function getSimilarArtistsTo(artist, callback){
    const url = '/api/lastfm/similarartists?artist=' + artist;
    fetch(url)
        .then(response => {
            if (response.ok)
                return response.json()
            else
                callback(response.json())
        })
        .then( (data) => {
            callback(null, data)
        })
        .catch( (err) => { //network error
            callback(err)
        })
}

exports.getGenresOf = function getGenresOf(artist, callback){
    const url = '/api/lastfm/genres?artist=' + artist;
    fetch(url)
        .then(response => {
            if (response.ok)
                return response.json()
            else
                callback(response.json())
        })
        .then( (data) => {
            callback(null, data)
        })
        .catch( (err) => { //network error
            callback(err)
        })
}
